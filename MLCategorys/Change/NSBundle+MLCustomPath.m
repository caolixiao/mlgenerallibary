//
//  NSBundle+MLCustomPath.m
//

#import "NSBundle+MLCustomPath.h"

@implementation NSBundle (MLCustomPath)

+ (NSBundle *) mlBundle {
    
    NSString *path = [[NSBundle  mainBundle] pathForResource:@"MLBundle" ofType:@"bundle"];
    
    NSBundle *bundle  =[NSBundle bundleWithPath:path];
    if(bundle==nil){
        
        for (NSBundle *_bundle in [NSBundle allFrameworks]) {
//            if([_bundle.resourcePath hasSuffix:@"libMLPlugin.framework"]){
                path = [_bundle pathForResource:@"MLBundle" ofType:@"bundle"];
                bundle  =[NSBundle bundleWithPath:path];
                break;
//            }
        }
    }
    return bundle;
}

+ (NSString *) pathNIB:(NSString *) name {
    return [[[NSBundle mlBundle] bundlePath] stringByAppendingFormat:@"/%@.nib", name];
}

+ (NSData *) dataNIB:(NSString *) name {
    return [NSData dataWithContentsOfFile:[NSBundle pathNIB:name]];
}

+ (NSString *) pathHtml:(NSString *) name {
    return [[[NSBundle mlBundle] bundlePath] stringByAppendingFormat:@"/html/%@.html", name];
}

+ (NSData *) dataHtml:(NSString *) name {
    return [NSData dataWithContentsOfFile:[NSBundle pathHtml:name]];
}


@end
