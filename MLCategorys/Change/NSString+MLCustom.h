//
//  NSString+MLCustom.h
//

#import <Foundation/Foundation.h>

@interface NSString (MLCustom)

#pragma mark - 
- (NSString *) trimmedString;


#pragma mark -
- (NSString *) stringFromElemnt;
- (NSMutableDictionary*) queryStringDictionary;

#pragma mark -
- (long long) findNumFromString;
- (float) findFloatFromString;

- (NSString *) getPureNumber;// 去掉号码后面的文字，获取号码

#pragma mark - 
- (CGFloat) widthWithSize:(CGFloat) fs;



+(NSString *) jsonStringWithDictionary:(NSDictionary *)dictionary;

+(NSString *) jsonStringWithArray:(NSArray *)array;

+(NSString *) jsonStringWithString:(NSString *) string;

+(NSString *) jsonStringWithObject:(id) object;


#pragma mark -

- (CGFloat) widthSize14;

- (CGFloat) heightSize14:(CGFloat) width;
- (CGFloat) heightWithSize:(CGFloat) fs width:(CGFloat) width;

- (CGFloat) heightAttributesWithSize:(CGFloat) fs width:(CGFloat) width;



- (NSData*) JSONString;

- (NSString *) delHtmlFormat;


@end
