//
//  NSBundle+MLCustomPath.h
//

#import <Foundation/Foundation.h>

@interface NSBundle (MLCustomPath)

+ (NSBundle *) mlBundle;

+ (NSString *) pathNIB:(NSString *) name;
+ (NSData *) dataNIB:(NSString *) name;

+ (NSString *) pathHtml:(NSString *) name;
+ (NSData *) dataHtml:(NSString *) name;

@end
