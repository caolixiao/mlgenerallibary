//
//  UIImage+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIImage (MLCustom)

#pragma mark -
// 毛玻璃

// view 转化成图片
+ (UIImage *)imageWithView:(UIView *) view;

// 自动缩放用的
+ (UIImage *) scaleImage:(UIImage *)image toScale:(float)scaleSize;

// 创建一个颜色图片
+ (UIImage *) imageNavWithColor:(UIColor *) color;
+ (UIImage *) imageWithColor:(UIColor *)color;
+ (UIImage *) imageWithColor:(UIColor *)color size:(CGSize)size;

#pragma mark - color
- (UIImage *) imageWithAlpha:(CGFloat) alpha;
- (UIImage *) imageLineWithColor:(UIColor *)color;

- (UIImage *) imageWithTintColor:(UIColor *) tintColor;
- (UIImage *) imageWithTintColor:(UIColor *)tintColor size:(CGSize)size;
- (UIImage *) imageWithFillColor:(UIColor *)tintColor size:(CGSize)size;

- (UIImage*) imageBlackToTransparentTintColor:(UIColor *)tintColor;

#pragma mark - size
- (UIImage *) scaleAuto;

- (UIImage *) scaleImageToSize:(CGSize)size;                     // 图片比例优先
- (UIImage *) rescaleImageToSize:(CGSize)size;                   // 图片大小重定义

#pragma mark - setting
- (UIImage *) clipImageToSize:(CGSize)size scale:(BOOL)scale;    // 图片裁剪并且缩放

- (UIImage *) cornerImageToRadius:(int)radius margin:(int)margin marginColor:(UIColor *)clolor; //图片圆角、边框


#pragma mark - load bundle Image
+ (UIImage *) imgNamed:(NSString *) name path:(NSString *) path;
+ (UIImage *) imgNamed:(NSString *) name;
+ (UIImage *) imgGNamed:(NSString *) name;
+ (UIImage *) imgKBNamed:(NSString *) name;

+ (UIImage *) imgGifNamed:(NSString *) name;
+ (NSArray *) imgsGifNamed:(NSString *) path;

@end
