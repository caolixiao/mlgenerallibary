//
//  UIImage+MLCustom.m
//

#import "UIImage+MLCustom.h"
#import "NSBundle+MLCustomPath.h"

@implementation UIImage (MLCustom)

#pragma mark -
+ (UIImage *)imageWithView:(UIView *) view {
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize

{
    CGFloat width = ((int)(image.size.width * scaleSize *2.0))/2.0;
    CGFloat height = ((int)(image.size.height * scaleSize *2.0))/2.0;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height),NO,[UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
    
}


+ (UIImage *)imageNavWithColor:(UIColor *) color
{
    return [UIImage imageWithColor:color size:CGSizeMake(375.0 * [NSObject resize_scale], (69.0f))];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    return [UIImage imageWithColor:color size:CGSizeMake(1, 1)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIImage *img = nil;
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   color.CGColor);
    CGContextFillRect(context, rect);
    
    img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark - color
- (UIImage *) imageWithAlpha:(CGFloat)alpha
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
    CGRect bounds = (CGRect){CGPointZero, self.size};
    [self drawInRect:bounds blendMode:kCGBlendModeOverlay alpha:alpha];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

- (UIImage *)imageLineWithColor:(UIColor *)color;
{
    UIImage *img = nil;
    
    CGRect rect = CGRectMake(0, 0, __SCREEN_SIZE.width, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    
    img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 2);
    
    [tintColor setFill];
    CGRect bounds = (CGRect){CGPointZero, self.size};
    UIRectFill(bounds);
    
    [self drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

void ProviderReleaseData (void *info, const void *data, size_t size)
{
    free((void*)data);
}


- (UIImage*) imageBlackToTransparentTintColor:(UIColor *)tintColor
{
    // 分配内存
    const int imageWidth = self.size.width;
    const int imageHeight = self.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), self.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++)
    {
        if ((*pCurPtr & 0x81bd2500) == 0x81bd2500)    // 将白色变成透明
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 230; //0~255
            ptr[2] = 115;
            ptr[1] = 111;
        }
        else if ((*pCurPtr & 0xFFFFFFff) == 0xffffffff)    // 将白色变成透明
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 255;
            ptr[2] = 255;
            ptr[1] = 255;
        }
        else
        {
            // 改成下面的代码，会将图片转成想要的颜色
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[0] = 0;
            //            ptr[3] = 230; //0~255
            //            ptr[2] = 115;
            //            ptr[1] = 111;
            
        }
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    [tintColor setFill];
    CGRect bounds = (CGRect){CGPointZero, size};
    UIRectFill(bounds);
    
    [self drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

- (UIImage *) imageWithFillColor:(UIColor *)tintColor size:(CGSize)size {
    CGFloat sourceWidth  = self.size.width;
    CGFloat sourceHeight = self.size.height;
    
    CGRect rect = CGRectMake((size.width - sourceWidth)/2.0, (size.height - sourceHeight)/2.0, sourceWidth, sourceHeight);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    
    [self drawInRect:rect];
    
    UIImage *_image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return _image;
    
}


#pragma mark - size
- (UIImage *) scaleAuto {
    return [UIImage scaleImage:self toScale:[NSObject resize_scale]];
}

-(UIImage *)scaleImageToSize:(CGSize)size{
    
    CGFloat sourceWidth  = self.size.width;
    CGFloat sourceHeight = self.size.height;
    
    CGFloat viewWidth  = size.width;
    CGFloat viewHeight = size.height;
    
    CGFloat outWidth  = viewWidth;
    CGFloat outHeight = viewHeight;
    
    CGFloat widthFactor = sourceWidth / viewWidth;
    CGFloat heightFactor = sourceHeight / viewHeight;
    
    CGFloat scaleFactor = widthFactor>heightFactor?widthFactor:heightFactor;
    outWidth =  sourceWidth / scaleFactor;
    outHeight =  sourceHeight / scaleFactor;
    
    CGRect rect = CGRectMake(0.0, 0.0, outWidth, outHeight);
    
    UIGraphicsBeginImageContextWithOptions(rect.size,NO,[UIScreen mainScreen].scale);
    
    [self drawInRect:rect];
    
    UIImage *_image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return _image;
}

- (UIImage *)rescaleImageToSize:(CGSize)size {
    CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size,NO,[UIScreen mainScreen].scale);
    [self drawInRect:rect];  // scales image to rect
    UIImage *resImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resImage;
}

#pragma mark - setting
- (UIImage *)clipImageToSize:(CGSize)size scale:(BOOL)scale{
    CGFloat sourceWidth  = self.size.width;
    CGFloat sourceHeight = self.size.height;
    
    CGFloat viewWidth  = size.width;
    CGFloat viewHeight = size.height;
    
    CGFloat outWidth  = viewWidth;
    CGFloat outHeight = viewHeight;
    
    CGFloat widthFactor = sourceWidth / viewWidth;
    CGFloat heightFactor = sourceHeight / viewHeight;
    
    CGFloat scaleFactor = widthFactor<heightFactor?widthFactor:heightFactor;
    
    outWidth  =  viewWidth * scaleFactor;
    outHeight =  viewHeight * scaleFactor;
    
    CGRect rect = CGRectMake((sourceWidth -outWidth )/2, (sourceHeight -outHeight )/2, outWidth,outHeight);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();//获取当前quartz 2d绘图环境
    
    CGContextTranslateCTM(currentContext, 0-rect.origin.x,sourceHeight-rect.origin.y);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    CGContextClipToRect( currentContext, rect);//设置当前绘图环境到矩形框
    
    CGRect _rect = CGRectMake(0.0, 0.0f, sourceWidth,sourceHeight);
    
    CGContextDrawImage(currentContext, _rect, self.CGImage);//绘图
    
    UIImage *_clipImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIImage *_image;
    
    if(scale){
        CGRect srect = CGRectMake(0.0, 0.0, viewWidth, viewHeight);
        UIGraphicsBeginImageContext(srect.size);
        [_clipImage drawInRect:srect];
        _image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }else{
        _image=_clipImage;
    }
    
    return [_image fixOrientation:self.imageOrientation];
}

-(UIImage *)fixOrientation:(UIImageOrientation)_imageOrientation{
    
    // No-op if the orientation is already correct
    if (_imageOrientation == UIImageOrientationUp) return self;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (_imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (_imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (_imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [[UIImage alloc] initWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

-(UIImage *)cornerImageToRadius:(int)radius margin:(int)margin marginColor:(UIColor *)clolor{
    
    CGFloat viewWidth  = self.size.width;
    CGFloat viewHeight = self.size.height;
    
    CGRect rect = CGRectMake(0, 0, viewWidth,viewHeight);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect rectToDraw = CGRectInset(rect, 0, 0);
    
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRoundedRect:rectToDraw byRoundingCorners:(UIRectCorner)UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
    
    /* Background */
    CGContextSaveGState(ctx);
    {
        CGContextAddPath(ctx, borderPath.CGPath);
        
        CGContextSetFillColorWithColor(ctx, clolor.CGColor);
        
        CGContextDrawPath(ctx, kCGPathFill);
    }
    CGContextRestoreGState(ctx);
    {
        
        CGRect _rect=CGRectMake(rect.origin.x+margin, rect.origin.y+margin, rect.size.width-2*margin, rect.size.height-2*margin);
        CGRect rectToDraw = CGRectInset(_rect, margin, margin);
        
        UIBezierPath *iconPath = [UIBezierPath bezierPathWithRoundedRect:rectToDraw byRoundingCorners:(UIRectCorner)UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
        
        /* Image and Clip */
        CGContextSaveGState(ctx);
        {
            CGContextAddPath(ctx, iconPath.CGPath);
            CGContextClip(ctx);
            
            CGContextTranslateCTM(ctx, 0.0, self.size.height);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            
            CGContextDrawImage(ctx, _rect, self.CGImage);
            
        }
        
    }
    
    UIImage *_clipImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    return _clipImage;
}


#pragma mark - load bundle Image
+(UIImage *) imgNamed:(NSString *) name path:(NSString *) path {
    if (!name && name.length == 0) return nil;
    
    NSBundle *bundle = [NSBundle mlBundle];
    
    if (__DEVICE_IPHONE_PLUS) {
        NSString *img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@3x", path, name] ofType:@"png"];
        if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@2x", path, name] ofType:@"png"];
        if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@", path, name] ofType:@"png"];
        if (!img_path) INFO_NSLOG(@"%@", img_path ? img_path : [NSString stringWithFormat:@"没有找到图片:%@", name]);
        return [UIImage imageWithContentsOfFile:img_path];
    }else{
        NSString *img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@2x", path, name] ofType:@"png"];
        if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@3x", path, name] ofType:@"png"];
        if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@", path, name] ofType:@"png"];
        if (!img_path) INFO_NSLOG(@"%@", img_path ? img_path : [NSString stringWithFormat:@"没有找到图片:%@", name]);
        return [UIImage imageWithContentsOfFile:img_path];
    }
}


+(UIImage *) imgNamed:(NSString *)name {
    return [UIImage imgNamed:name path:@"img"];
}

+ (UIImage *) imgGNamed:(NSString *)name {
    return [UIImage imgNamed:name path:@"general"];
}

+(UIImage *) imgKBNamed:(NSString *)name {
    return [UIImage imgNamed:name path:@"Keyboard"];
}


+(UIImage *) imgGifNamed:(NSString *)name {
    if (!name && name.length == 0) return nil;
    NSBundle *bundle = [NSBundle mlBundle];
    
    NSString *img_path = nil;
    if (__DEVICE_IPHONE_PLUS) img_path = [bundle pathForResource:[NSString stringWithFormat:@"gif/%@@3x", name] ofType:@"png"];
    else img_path = [bundle pathForResource:[NSString stringWithFormat:@"gif/%@@2x", name] ofType:@"png"];
    return [UIImage imageWithContentsOfFile:img_path];
}


+ (NSArray *) imgsGifNamed:(NSString *) path {
    if (!path) return nil;
    NSBundle *bundle = [NSBundle mlBundle];
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/gif/", bundle.resourcePath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *testDirectory = [documentsDirectory stringByAppendingPathComponent:path];
    NSArray *array = [fileManager contentsOfDirectoryAtPath:testDirectory error:NULL];
    NSMutableArray *__array = [NSMutableArray array];
    for (NSString *str in array) {
        if (__DEVICE_IPHONE_PLUS) {
            if ([str isEqualToString:@".DS_Store"] || ![str hasSuffix:@"@2x.png"]) continue;
            
            NSString *tmp = [NSString stringWithFormat:@"%@/%@", testDirectory, [str stringByReplacingOccurrencesOfString:@"@3x" withString:@""]];
            [__array addObject:[UIImage imageWithContentsOfFile:tmp]];
        }else{
            if ([str isEqualToString:@".DS_Store"] || ![str hasSuffix:@"@3x.png"]) continue;
            
            NSString *tmp = [NSString stringWithFormat:@"%@/%@", testDirectory, [str stringByReplacingOccurrencesOfString:@"@2x" withString:@""]];
            [__array addObject:[UIImage imageWithContentsOfFile:tmp]];
        }
    }
    
    return __array.count == 0 ? nil : __array;
}

@end
