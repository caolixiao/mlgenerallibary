//
//  UIView+MLCustom.m
//

#import "UIView+MLCustom.h"
#import <objc/runtime.h>
#import <CommonCrypto/CommonDigest.h>

@implementation UIView (MLCustom)


#pragma mark - UIWindow
+ (UIWindow *) getTopWindow
{
    UIWindow * topWindows = [UIApplication sharedApplication].keyWindow;
    for (UIWindow *window in  [UIApplication sharedApplication].windows) {
        if(topWindows.windowLevel<window.windowLevel){
            topWindows =window;
        }
    }
    return topWindows;
}

#pragma mark - UIViewController
- (UIViewController *) viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

- (UIViewController *) currentViewController {
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    if (window.subviews.count == 0) return nil;
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UINavigationController class]])
        result = [(UINavigationController *)nextResponder visibleViewController];
    else if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

#pragma mark -
static const char *__LX__ObjcInfo__ = "__LX__ObjcInfo__";

- (void) setObjcInfo:(id)objcInfo
{
    objc_setAssociatedObject(self, __LX__ObjcInfo__, objcInfo, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) objcInfo
{
    return objc_getAssociatedObject(self, __LX__ObjcInfo__);
}


@end
