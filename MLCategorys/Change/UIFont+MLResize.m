//
//  UIFont+Resize.m 
//

#import "UIFont+MLResize.h"

@implementation UIFont (MLResize)

+ (UIFont *)systemFontOfBaseSize:(CGFloat)fontSize{
    fontSize = fontSize * [self resize_scale];
    return [UIFont systemFontOfSize:fontSize];

}

+ (UIFont *)boldSystemFontOfBaseSize:(CGFloat)fontSize{
    fontSize = fontSize * [self resize_scale];
    return [UIFont boldSystemFontOfSize:fontSize];
}

@end
