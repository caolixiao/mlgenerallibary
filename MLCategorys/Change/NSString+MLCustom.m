//
//  NSString+MLCustom.m
//

#import "NSString+MLCustom.h"

@implementation NSString (MLCustom)

#pragma mark -
- (NSString *)trimmedString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


#pragma mark -
- (NSMutableDictionary*) queryStringDictionary {
    @try {
        NSArray *list = [self componentsSeparatedByString:@"&"];
        NSMutableDictionary *val = [NSMutableDictionary dictionaryWithCapacity:list.count];
        for(NSString *e in list) {
            NSArray *content = [e componentsSeparatedByString:@"="];
            [val setObject:[content objectAtIndex:1] forKey:[content objectAtIndex:0]];
        }
        return val;
    } @catch (NSException *exception) {
        return nil;
    }
}

- (NSString *) stringFromElemnt {
    NSRange range1=[self rangeOfString:@"\"" options:NSCaseInsensitiveSearch];
    NSRange range2=[self rangeOfString:@"&" options:NSCaseInsensitiveSearch];
    if(range1.location == NSNotFound && range2.location == NSNotFound) return self;
    
    if (range1.location != NSNotFound && range2.location != NSNotFound) {
        if (range1.location < range2.location) return [self substringToIndex:range1.location];
        else return [self substringToIndex:range2.location];
    }
    
    if (range2.location != NSNotFound) return [self substringToIndex:range2.location];
    if (range1.location != NSNotFound) return [self substringToIndex:range1.location];
    
    return nil;
}


#pragma mark -
- (long long) findNumFromString
{
    if (!self) return 0;
    NSMutableString *numString = [[NSMutableString alloc] init];
    NSString *tempStr;
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *nums = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while (![scanner isAtEnd]) {
        [scanner scanUpToCharactersFromSet:nums intoString:NULL];
        
        [scanner scanCharactersFromSet:nums intoString:&tempStr];
        [numString appendString:tempStr];
        tempStr = @"";
    }
    
    long long num = [numString integerValue];
    
    return num;
}

- (float) findFloatFromString
{
    if (!self) return -1;
    NSMutableString *fString = [[NSMutableString alloc] init];
    NSString *tempStr;
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *nums = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    while (![scanner isAtEnd]) {
        [scanner scanUpToCharactersFromSet:nums intoString:NULL];
        
        [scanner scanCharactersFromSet:nums intoString:&tempStr];
        [fString appendString:tempStr];
        tempStr = @"";
    }
    
    return  [fString floatValue];
}

- (NSString *)getPureNumber{
    NSError *error;
    // 创建NSRegularExpression对象并指定正则表达式
    NSString * reg = @"[0-9]|-";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:reg
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSMutableString * phone = [NSMutableString string];
    if (!error) { // 如果没有错误
        // 获取特特定字符串的范围
        NSArray * match = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
        if (match) {
            // 截获特定的字符串
            for (int i =0 ;i < match.count ;i++) {
                NSTextCheckingResult * result = match[i];
                NSRange range = [result range];
                NSString *mStr = [self substringWithRange:range];
                [phone appendString:mStr];
            }
            NSLog(@"截获特定的字符串 = %@",phone);
        }
    } else { // 如果有错误，则把错误打印出来
        NSLog(@"error - %@", error);
    }
    
    return phone;
}

#pragma mark -
- (CGFloat) widthWithSize:(CGFloat) fs
{
    UIFont *font = [UIFont systemFontOfSize:fs];
    CGSize size = CGSizeMake(1000.0f, fs + 2.0f);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:tdic context:nil].size;
    return size.width + 3.0f;
}



#pragma mark -

+(NSString *) jsonStringWithString:(NSString *) string{
    return [NSString stringWithFormat:@"\"%@\"",
            [[string stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"] stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""]
            ];
}

+(NSString *) jsonStringWithArray:(NSArray *)array{
    NSMutableString *reString = [NSMutableString string];
    [reString appendString:@"["];
    NSMutableArray *values = [NSMutableArray array];
    for (id valueObj in array) {
        NSString *value = [NSString jsonStringWithObject:valueObj];
        if (value) {
            [values addObject:[NSString stringWithFormat:@"%@",value]];
        }
    }
    [reString appendFormat:@"%@",[values componentsJoinedByString:@","]];
    [reString appendString:@"]"];
    return reString;
}

+(NSString *) jsonStringWithDictionary:(NSDictionary *)dictionary{
    NSArray *keys = [dictionary allKeys];
    NSMutableString *reString = [NSMutableString string];
    [reString appendString:@"{"];
    NSMutableArray *keyValues = [NSMutableArray array];
    for (int i=0; i<[keys count]; i++) {
        NSString *name = [keys objectAtIndex:i];
        id valueObj = [dictionary objectForKey:name];
        NSString *value = [NSString jsonStringWithObject:valueObj];
        if (value) {
            [keyValues addObject:[NSString stringWithFormat:@"\"%@\":%@",name,value]];
        }
    }
    [reString appendFormat:@"%@",[keyValues componentsJoinedByString:@","]];
    [reString appendString:@"}"];
    return reString;
}

+(NSString *) jsonStringWithObject:(id) object{
    NSString *value = nil;
    if (!object) {
        return value;
    }
    if ([object isKindOfClass:[NSString class]]) {
        value = [NSString jsonStringWithString:object];
    }else if([object isKindOfClass:[NSDictionary class]]){
        value = [NSString jsonStringWithDictionary:object];
    }else if([object isKindOfClass:[NSArray class]]){
        value = [NSString jsonStringWithArray:object];
    }
    return value;
}


#pragma mark -
- (CGFloat) widthSize14
{
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    CGSize size = CGSizeMake(1000.0f, 16.0f);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:tdic context:nil].size;
    return size.width + 3.0f;
}


- (CGFloat) heightSize14:(CGFloat) width
{
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    CGSize size = CGSizeMake(width, 10000.0f);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:tdic context:nil].size;
    return size.height + 2.0f;
}

- (CGFloat) heightWithSize:(CGFloat) fs width:(CGFloat) width
{
    UIFont *font = [UIFont systemFontOfSize:fs];
    CGSize size = CGSizeMake(width, 1000.0f);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:tdic context:nil].size;
    return size.height + 2.0f;
}

- (CGFloat) heightAttributesWithSize:(CGFloat) fs width:(CGFloat) width
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:fs]};
    CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
    return rect.size.height + 2.0f;
}


- (NSData*) JSONString
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    if (error != nil) return nil;
    return result;
}


#pragma mark -
- (NSString *) delHtmlFormat {
    NSRegularExpression *regularExpretion=[NSRegularExpression regularExpressionWithPattern:@"<[^>]*>|\n" options:0 error:nil];
    return [regularExpretion stringByReplacingMatchesInString:self options:NSMatchingReportProgress range:NSMakeRange(0, self.length) withTemplate:@""];
}



 @end
