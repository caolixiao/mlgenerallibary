//
//  UIView+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIView (MLCustom)

#pragma mark - UIWindow
+ (UIWindow *) getTopWindow;

#pragma mark - UIViewController 
- (UIViewController *) viewController;
- (UIViewController *) currentViewController;

#pragma mark -
@property (nonatomic, strong) id objcInfo;

@end
