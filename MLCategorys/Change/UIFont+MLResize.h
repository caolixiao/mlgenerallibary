//
//  UIFont+MLResize.h 
//

#import <UIKit/UIKit.h>

@interface UIFont (MLResize)

+ (UIFont *) systemFontOfBaseSize:(CGFloat)fontSize;

+ (UIFont *) boldSystemFontOfBaseSize:(CGFloat)fontSize;

@end
