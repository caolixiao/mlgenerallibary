//
//  UIButton+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIButton (MLCustom)

- (void) setBackgroundColor:(UIColor *)color forState:(UIControlState)state;

- (void) setCustomStyle;
- (void) setIsEnabled:(BOOL) enabled;

- (void) startCountDown:(int) len;

@end
