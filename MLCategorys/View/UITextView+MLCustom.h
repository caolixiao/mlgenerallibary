//
//  UITextView+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UITextView (MLCustom)

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UILabel *labPlaceholder;

@end
