//
//  UIButton+MLCustom.m
//

#import "UIButton+MLCustom.h"
#import "UIImage+MLCustom.h"

@implementation UIButton (MLCustom)

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state
{
    [self setBackgroundImage:[UIImage imageWithColor:color] forState:state];
}


- (void) setCustomStyle {
    [self setIsEnabled:NO];
    
    self.layer.cornerRadius = 4.0f;
    self.layer.masksToBounds = YES;
    self.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self setTitleColor:__RGB(0xffffff) forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageWithColor:COLOR_GENERAL size:self.bounds.size] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageWithColor:[COLOR_GENERAL colorWithAlphaComponent:0.6] size:self.bounds.size] forState:UIControlStateDisabled];
    [self setBackgroundImage:[UIImage imageWithColor:[COLOR_GENERAL colorWithAlphaComponent:0.6] size:self.bounds.size] forState:UIControlStateSelected];
}

- (void) setIsEnabled:(BOOL) enabled {
    if (enabled) {
        self.userInteractionEnabled = YES;
        self.selected = NO;
    }
    else {
        self.userInteractionEnabled = NO;
        self.selected = YES;
    }
}

- (void) startCountDown:(int) len {
    self.enabled = NO;
    __weak typeof(self) weakSelf = self;
    __block int _time = len;
    __weak NSString *title = self.titleLabel.text;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        [weakSelf setTitle:[NSString stringWithFormat:@"%d秒", _time] forState:UIControlStateDisabled];
        _time--;
        if (_time == 0) dispatch_source_cancel(timer);
    });
    dispatch_source_set_cancel_handler(timer, ^{
        [weakSelf setTitle:title forState:UIControlStateDisabled];
        weakSelf.enabled = YES;
    });
    dispatch_resume(timer);
}



@end
