//
//  UITextField+MLCustom.m
//

#import "UITextField+MLCustom.h"
#import <objc/message.h>
#import <CommonCrypto/CommonDigest.h>

@implementation UITextField (MLCustom)

-(void) setStyleWithLeftViewTitle:(NSString *)title {
    [self leftTitle:title w:0];
    [self style_Default1];
}

- (void) leftTitle:(NSString *) title w:(CGFloat) fW {
    self.leftView = nil;

    UILabel *labTitle        = [[UILabel alloc] init];
    labTitle.backgroundColor = [UIColor clearColor];
    labTitle.font            = [UIFont systemFontOfSize:13.0f];
    labTitle.numberOfLines   = 1;
    labTitle.textColor       = __RGB(0x8b8b8b);
    labTitle.textAlignment   = NSTextAlignmentLeft;
//    labTitle.text            = [NSString stringWithFormat:@"   手机号码: "];
    labTitle.text            = [NSString stringWithFormat:@"   %@ ", title];
    [labTitle sizeToFit];
    if (fW > 0) labTitle.frame           = CGRectMake(0.0f, 0.0f, fW, 44.0f);
    else [labTitle sizeToFit];
    
    self.leftView = labTitle;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (void) style_Default1 {
    //    self.borderStyle = UITextBorderStyleBezel;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = __RGB(0x9e9e9e).CGColor;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 6.0f;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = __RGB(0x9e9e9e);
    self.font = [UIFont systemFontOfSize:13.0f];

}

- (void) style_Default2 {
//    self.layer.borderWidth = 0.5f;
//    self.layer.borderColor = __RGB(0x9e9e9e).CGColor;
//    self.layer.masksToBounds = YES;
//    self.layer.cornerRadius = 6.0f;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = __RGB(0x9e9e9e);
    self.font = [UIFont systemFontOfSize:13.0f];
}



- (void) addRightEye
{
    self.secureTextEntry = YES;
    UIButton *butEye = [UIButton buttonWithType:UIButtonTypeCustom];
    butEye.frame = CGRectMake(0.0f, 0.0f, 35.0f, 21.0f);
    [butEye setImage:[UIImage imageNamed:@"password-show-0"] forState:UIControlStateNormal];
    [butEye setImage:[UIImage imageNamed:@"password-blank-0"] forState:UIControlStateDisabled];
    [butEye setImage:[UIImage imageNamed:@"password-blank-0"] forState:UIControlStateSelected];
    butEye.selected = YES;
    [butEye addTarget:self action:@selector(actionEyeWithButton:) forControlEvents:UIControlEventTouchUpInside];
    self.rightView = butEye;
    self.rightViewMode = UITextFieldViewModeAlways;
}

- (void) actionEyeWithButton:(UIButton *) button {
    button.selected = !button.selected;
    self.secureTextEntry = button.selected;
}

#pragma mark -

@dynamic ImgHeight;
@dynamic textlength;
@dynamic limit;


static const char *__LX__ImgHeight__ = "__LX__ImgHeight__";
static const char *__LX__Limit__ = "__LX__Limit__";
static const char *__LX__TextLength__ = "__LX__TextLength__";


- (void) setImgHeight:(CGFloat)ImgHeight
{
    objc_setAssociatedObject(self, __LX__ImgHeight__, [NSNumber numberWithFloat:ImgHeight], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGFloat) ImgHeight
{
    return [objc_getAssociatedObject(self, __LX__ImgHeight__) floatValue];
}

- (void) setTextlength:(int)textlength
{
    objc_setAssociatedObject(self, __LX__TextLength__, [NSNumber numberWithInt:textlength], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int) textlength
{
    return [objc_getAssociatedObject(self, __LX__TextLength__) intValue];
}

- (void) setLimit:(LimitLength)limit
{
    objc_setAssociatedObject(self, __LX__Limit__, limit, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (LimitLength) limit
{
    return objc_getAssociatedObject(self, __LX__Limit__);
}


#pragma mark - methon
+(UITextField *)textFieldPlace:(NSString *)text leftView:(UIImage *)image{
    UITextField *textField = [[UITextField alloc]init];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField setLeftImg:image];
    textField.placeholder = text;
    return textField;
}

#pragma mark - 

- (void) setLeftImg:(UIImage *) img
{
    UIImageView *iv = [[UIImageView alloc] initWithImage:img];
    iv.contentMode = UIViewContentModeCenter;
    iv.frame = CGRectMake(0.0f, 0.0f, 38.0f, 38.0f);
    self.leftView = iv;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    self.ImgHeight = 38.0f;
}

- (void) setRightImg:(UIImage *) img
{
    UIImageView *iv = [[UIImageView alloc] initWithImage:img];
    iv.contentMode = UIViewContentModeCenter;
    iv.frame = CGRectMake(0.0f, 0.0f, 38.0f, 38.0f);
    self.rightView = iv;
    self.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
- (void) setlimitLength:(int) length limit:(LimitLength) limit
{
    self.textlength = length;
    self.limit = limit;
    [self addTarget:self action:@selector(limitLength:) forControlEvents:UIControlEventEditingChanged];
}

- (void) limitLength:(UITextField *) tfLimit {
    BOOL isChinese;//判断当前输入法是否是中文
    BOOL islimit = NO;
    
    if ([tfLimit.textInputMode.primaryLanguage isEqualToString: @"zh_CN"]) isChinese = YES; else isChinese = NO;
    
    NSString *str = [[tfLimit text] stringByReplacingOccurrencesOfString:@"?" withString:@""];
    if (isChinese) { //中文输入法下
        UITextRange *selectedRange = [tfLimit markedTextRange];
        UITextPosition *position = [tfLimit positionFromPosition:selectedRange.start offset:0];
        if (!position) {
            if (str.length>self.textlength) {
                NSString *strNew = [NSString stringWithString:str];
                [tfLimit setText:[strNew substringToIndex:self.textlength]];
                islimit = YES;
            }
        }
    }else{
        if ([str length]>self.textlength) {
            NSString *strNew = [NSString stringWithString:str];
            [tfLimit setText:[strNew substringToIndex:self.textlength]];
            islimit = YES;
        }
    }
    
    if (islimit) {
        if (self.limit) self.limit(self, self.textlength);
    }
}

@end
