//
//  UITextView+MLCustom.m
//

#import "UITextView+MLCustom.h"
#import <objc/message.h>

@implementation UITextView (MLCustom)

@dynamic placeholder;
@dynamic labPlaceholder;

#pragma mark -
static const char *__LX__labPlaceholder__ = "__LX__labPlaceholder__";
static const char *__LX__placeholder__ = "__LX__placeholder__";

- (void) setLabPlaceholder:(UILabel *)labPlaceholder
{
    objc_setAssociatedObject(self, __LX__labPlaceholder__, labPlaceholder, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *) labPlaceholder
{
   return objc_getAssociatedObject(self, __LX__labPlaceholder__);
}

- (void) setPlaceholder:(NSString *)placeholder
{
    objc_setAssociatedObject(self, __LX__placeholder__, placeholder, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    if (!self.labPlaceholder) {
        self.labPlaceholder = [[UILabel alloc] init];
        self.labPlaceholder.exclusiveTouch = true;
        self.labPlaceholder.textColor = __RGB(0xd0d0d9);
        self.labPlaceholder.backgroundColor = [UIColor clearColor];
        self.labPlaceholder.font = [UIFont systemFontOfSize:13.0];
        self.labPlaceholder.numberOfLines = 1;
        [self addSubview:self.labPlaceholder];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionDidBeginEditing) name:UITextViewTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionDidEndEditing) name:UITextViewTextDidEndEditingNotification object:nil];
    }
    
    if (placeholder == nil) {
        self.labPlaceholder = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidEndEditingNotification object:nil];
    }
    
    self.labPlaceholder.frame = CGRectMake(3, 7.0, VIEWW(self), CGScale(16.0));
    self.labPlaceholder.text = placeholder;
}

- (NSString *) placeholder
{
    return objc_getAssociatedObject(self, __LX__placeholder__);
}

- (void) actionDidBeginEditing {
    self.labPlaceholder.text = @"";
}

- (void) actionDidEndEditing {
    if (!self.text || self.text.length == 0) self.labPlaceholder.text = self.placeholder;
}

@end
