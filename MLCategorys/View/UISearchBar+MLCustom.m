//
//  UISearchBar+MLCustom.m
//

#import "UISearchBar+MLCustom.h"

@implementation UISearchBar (MLCustom)

- (void)setSearchTextFieldBKG:(UIColor *) bkg fontColor:(UIColor *) fontColor placeholder:(NSString *) placeholder {
    self.backgroundImage = [UIImage imageNavWithColor:__RGB(0xf5f5f5)];
    
    UITextField *searchTextField = nil;
    if (!__IPHONE_OS_VERSION_6X_LAST) {
        // 经测试, 需要设置barTintColor后, 才能拿到UISearchBarTextField对象
        self.barTintColor = bkg;
        self.tintColor = COLOR_GENERAL;
        searchTextField = [[[self.subviews firstObject] subviews] lastObject];
    } else { // iOS6以下版本searchBar内部子视图的结构不一样
        for (UITextField *subView in self.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                searchTextField = subView;
            }
        }
    }
    
    //    searchTextField.layer.cornerRadius = 13.0f;
    searchTextField.backgroundColor = bkg;
    
    searchTextField.textColor = fontColor;
    searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: fontColor}];
    //    [searchTextField setValue:fontColor forKeyPath:@"_placeholderLabel.textColor"];
    
    //
    UIImageView *iv = (UIImageView *)searchTextField.leftView;
    iv.image = [iv.image imageWithTintColor:COLOR_GENERAL];
}

@end
