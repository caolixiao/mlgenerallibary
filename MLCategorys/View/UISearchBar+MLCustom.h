//
//  UISearchBar+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UISearchBar (MLCustom)

- (void)setSearchTextFieldBKG:(UIColor *) bkg fontColor:(UIColor *) fontColor placeholder:(NSString *) placeholder;

@end
