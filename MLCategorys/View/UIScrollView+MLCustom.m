//
//  UIScrollView+MLCustom.m
//

#import "UIScrollView+MLCustom.h"
#import <objc/message.h>
#import <CommonCrypto/CommonDigest.h>

@implementation UIScrollView (MLCustom)

@dynamic tap;
@dynamic cancelui;

static const char *__LX__Tap__ = "__LX__Tap__";
- (void) setTap:(UITapGestureRecognizer *)tap
{
    objc_setAssociatedObject(self, __LX__Tap__, tap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITapGestureRecognizer *) tap
{
    return objc_getAssociatedObject(self, __LX__Tap__);
}

static const char *__LX__Cancel_UI__ = "__LX__Cancel_UI__";

- (void) setCancelui:(UIResponder *)cancelui
{
    objc_setAssociatedObject(self, __LX__Cancel_UI__, cancelui, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIResponder *) cancelui
{
    return objc_getAssociatedObject(self, __LX__Cancel_UI__);
}

- (void) addKeyboardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillhide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelKeyboard)];
    self.tap.cancelsTouchesInView = NO;
    [self.superview addGestureRecognizer:self.tap];
}

- (void) removeKeyboardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [self cancelKeyboard];
    [self.superview removeGestureRecognizer:self.tap];
}

#pragma mark - NSNotification post event
- (void) keyboardWillShow:(NSNotification*) notification
{
    NSDictionary* info = [notification userInfo];
    CGFloat keyboardHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
    self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
}

- (void) keyboardWillhide:(NSNotification*) notification
{
    self.contentInset = UIEdgeInsetsZero;
    self.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void) cancelKeyboard {
    [self endEditing:YES];
    
    if (self.cancelui && [self.cancelui isFirstResponder]) [self.cancelui resignFirstResponder];
}


@end
