//
//  UITextField+MLCustom.h
//

#import <UIKit/UIKit.h>

typedef void(^LimitLength)(id target, int index);


#pragma mark -
@interface UITextField (MLCustom)

- (void) setStyleWithLeftViewTitle:(NSString *)title;
- (void) leftTitle:(NSString *) title w:(CGFloat) fW;

- (void) style_Default1;
- (void) style_Default2;

- (void) addRightEye;


#pragma mark - arguments
@property (nonatomic, assign) CGFloat ImgHeight;
@property (nonatomic, assign) int textlength;
@property (nonatomic, strong) LimitLength limit;


#pragma mark - methon
+ (UITextField *) textFieldPlace:(NSString *) text leftView:(UIImage *) image;

- (void) setLeftImg:(UIImage *) img;
- (void) setRightImg:(UIImage *) img;

- (void) setlimitLength:(int) length limit:(LimitLength) limit;

@end
