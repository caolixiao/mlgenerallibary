//
//  UIScrollView+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIScrollView (MLCustom)

@property (nonatomic, strong) UITapGestureRecognizer *tap;

@property (nonatomic, strong) UIResponder *cancelui;

- (void) addKeyboardNotification;

- (void) removeKeyboardNotification;

@end
