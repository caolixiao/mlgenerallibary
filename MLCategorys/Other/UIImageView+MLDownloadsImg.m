//
//  UIImageView+MLDownloadsImg.m
//

#import "UIImageView+MLDownloadsImg.h"

@implementation UIImageView (MLMLDownloadsImg)

- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image cache:(BOOL) isCache {
    self.image = image;
    
    if (!url) return;
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    if (isCache) {
        NSString *fileName = [url lastPathComponent];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
        NSString *imgPath = [path stringByAppendingFormat:@"/%@", fileName];
        
        if ([fileManager fileExistsAtPath:imgPath]) self.image = [UIImage imageWithContentsOfFile:imgPath];
        else [self performSelectorInBackground:@selector(startMLDownloadsImgFromURL:) withObject:url];
    }else {
        [self performSelectorInBackground:@selector(startMLDownloadsImgFromURL:) withObject:url];
    }
}

- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image {
    [self loadImgFromUrl:url defaultImage:image cache:true];
}

- (void) loadImgFromUrl:(NSString*) url complate:(MLDownLoadsImgComplate) comp {
    if (!url) return;
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSString *fileName = [url lastPathComponent];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
    NSString *imgPath = [path stringByAppendingFormat:@"/%@", fileName];
    
    if ([fileManager fileExistsAtPath:imgPath]) {
        if (comp) comp([UIImage imageWithContentsOfFile:imgPath]);
        comp = nil;
    }
    else {
        [self performSelectorInBackground:@selector(startMLDownloadsImgFromURL:) withObject:@[url, comp]];
    }

}

- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image begin:(void (^)()) isBegin complate:(MLDownLoadsImgComplate) comp {
    if (!url) return;
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSString *fileName = [url lastPathComponent];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
    NSString *imgPath = [path stringByAppendingFormat:@"/%@", fileName];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        if (isBegin) isBegin();
//        [self performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:YES];
//        
//        [NSThread sleepForTimeInterval:2];
//        
//        if ([fileManager fileExistsAtPath:imgPath]) {
//            if (comp) comp([UIImage imageWithContentsOfFile:imgPath]);
//        }else {
//            if (comp) comp(image);
//        }
//    });
//    return;

    
    if ([fileManager fileExistsAtPath:imgPath]) {
        if (comp) comp([UIImage imageWithContentsOfFile:imgPath]);
        comp = nil;
    }
    else {
        if (isBegin) isBegin();
        self.image = image;
        [self performSelectorInBackground:@selector(startMLDownloadsImgFromURL:) withObject:@[url, comp]];
    }
}

- (void) startMLDownloadsImgFromURL: (id) args {
    NSURL *imageUrl = nil;
    MLDownLoadsImgComplate comp;
    if ([args isKindOfClass:[NSArray class]]) {
        NSArray *tmp = args;
        if (tmp.count > 0) imageUrl = [NSURL URLWithString:args[0]];
        if (tmp.count > 1) comp = args[1];
    }else {
        imageUrl = [NSURL URLWithString:args];
    }
    NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
    if (imageData) [self performSelectorOnMainThread:@selector(updateImgView:) withObject:(comp ? @[imageUrl, imageData, comp] : @[imageUrl, imageData])  waitUntilDone:NO];
}

-(void) updateImgView:(NSArray*) array {
    
    NSString *fileName = [[array objectAtIndex:0] lastPathComponent];
    NSData *data = [array objectAtIndex:1];
    MLDownLoadsImgComplate comp = array.count > 2 ? array[2] : nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
    NSString *imgPath = [path stringByAppendingFormat:@"/%@", fileName];
    
    if (![fileManager fileExistsAtPath:imgPath]) {
        // 不从在
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:imgPath contents:data attributes:nil];
    }
    
    if (comp) comp([UIImage imageWithData:data]);
    else self.image = [UIImage imageWithData:data];
    comp = nil;
}


#pragma mark - image
/**
 *  @brief 保存缓冲到指定位置
 *  @param img 图片数据
 *  @return 返回保存的路径
 */
+ (NSString *) saveImage:(UIImage *) img
{
    
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    NSString *name = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
    
    NSData *imgData = UIImageJPEGRepresentation(img, 0.5);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
    NSString *imgPath = [path stringByAppendingFormat:@"/%@.png", name];
    [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    [fileManager createFileAtPath:imgPath contents:imgData attributes:nil];
    
    return imgPath;
}

@end
