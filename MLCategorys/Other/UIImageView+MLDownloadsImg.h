//
//  UIImageView+MLDownloadsImg.h
//

#import <UIKit/UIKit.h>

typedef void (^MLDownLoadsImgComplate) (id target);

@interface UIImageView (MLDownloadsImg)

- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image cache:(BOOL) isCache;
- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image;
- (void) loadImgFromUrl:(NSString*) url complate:(MLDownLoadsImgComplate) comp;
- (void) loadImgFromUrl:(NSString*) url defaultImage:(UIImage *) image begin:(void (^)()) isBegin complate:(MLDownLoadsImgComplate) comp;


#pragma mark -
/**
 *  @brief 保存缓冲到指定位置
 *  @param img 图片数据
 *  @return 返回保存的路径
 */
+ (NSString *) saveImage:(UIImage *) img;


@end
