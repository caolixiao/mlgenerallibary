//
//  NSObject+MergeData.m
//

#import "NSObject+MergeData.h"
#import <objc/message.h>
#import <objc/runtime.h>
#import <CommonCrypto/CommonDigest.h>

#pragma mark - MergeData
@implementation NSObject (MergeData)

+ (void) copyObject:(id) src toDest:(id) dest
{
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList([src class], &outCount);
    for(int i=0;i<outCount;i++)
    {
        @synchronized(src) {
            objc_property_t property = properties[i];
            NSString *key = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
            NSString *method=[NSString stringWithFormat:@"set%@%@:", [[key substringToIndex:1] uppercaseString], [key substringFromIndex:1]];
            if(![src respondsToSelector:NSSelectorFromString(key)] || ![dest respondsToSelector:NSSelectorFromString(method)]) continue;
            
            NSString *pType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
            
            if ([pType hasPrefix:@"T@"]) {
                id value = ((id(*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSMutableString class]] ||
                    [value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSMutableArray class]] ||
                    [value isKindOfClass:[NSData class]] || [value isKindOfClass:[NSMutableData class]] ||
                    [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] ||
                    [value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSNull class]] ||
                    !value)
                {
                    ((void(*)(id, SEL, id))objc_msgSend)(dest, NSSelectorFromString(method), value);
                }
                else if (!value) continue;
                else
                {
                    id destValue = ((id(*)(id, SEL))objc_msgSend)(dest, NSSelectorFromString(key));
                    [NSObject copyObject:value toDest:destValue];
                }
                
            }
            else if([pType hasPrefix:@"T{"])
            {        }
            else
            {
                pType = [pType lowercaseString];
                if ([pType hasPrefix:@"ti"] || [pType hasPrefix:@"tb"])
                {
                    int destValue = ((int (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, int))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if ([pType hasPrefix:@"tf"])
                {
                    float destValue = ((float (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, float))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"td"])
                {
                    double destValue = ((double (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, double))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"tl"] || [pType hasPrefix:@"tq"])
                {
                    long long destValue = ((long long (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, long long))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if ([pType hasPrefix:@"tc"])
                {
                    char destValue = ((char (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, char))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"ts"])
                {
                    short destValue = ((short (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, short))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
            }
        }
    }
    free(properties);
}


+ (void) copyObject:(id) src toDest:(id) dest transients:(NSArray *) transients {
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList([src class], &outCount);
    for(int i=0;i<outCount;i++)
    {
        @synchronized(src) {
            objc_property_t property = properties[i];
            NSString *key = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
            NSString *method=[NSString stringWithFormat:@"set%@%@:", [[key substringToIndex:1] uppercaseString], [key substringFromIndex:1]];
            
            BOOL isTransients = NO;
            if (transients) {
                NSPredicate *pTransients = [NSPredicate predicateWithFormat:@"SELF=%@", key];
                NSArray *results = [transients filteredArrayUsingPredicate:pTransients];
                isTransients = (results.count != 0);
            }
            
            if(![src respondsToSelector:NSSelectorFromString(key)] || ![dest respondsToSelector:NSSelectorFromString(method)] || isTransients) continue;
            
            
            NSString *pType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
            
            if ([pType hasPrefix:@"T@"]) {
                id value = ((id(*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSMutableString class]] ||
                    [value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSMutableArray class]] ||
                    [value isKindOfClass:[NSData class]] || [value isKindOfClass:[NSMutableData class]] ||
                    [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] ||
                    [value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSNull class]] ||
                    !value)
                {
                    ((void(*)(id, SEL, id))objc_msgSend)(dest, NSSelectorFromString(method), value);
                }
                else if (!value) continue;
                else
                {
                    id destValue = ((id(*)(id, SEL))objc_msgSend)(dest, NSSelectorFromString(key));
                    [NSObject copyObject:value toDest:destValue];
                }
                
            }
            else if([pType hasPrefix:@"T{"])
            {        }
            else
            {
                pType = [pType lowercaseString];
                if ([pType hasPrefix:@"ti"] || [pType hasPrefix:@"tb"])
                {
                    int destValue = ((int (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, int))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if ([pType hasPrefix:@"tf"])
                {
                    float destValue = ((float (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, float))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"td"])
                {
                    double destValue = ((double (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, double))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"tl"] || [pType hasPrefix:@"tq"])
                {
                    long long destValue = ((long long (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, long long))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if ([pType hasPrefix:@"tc"])
                {
                    char destValue = ((char (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, char))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
                else if([pType hasPrefix:@"ts"])
                {
                    short destValue = ((short (*)(id, SEL))objc_msgSend)(src, NSSelectorFromString(key));
                    ((void(*)(id, SEL, short))objc_msgSend)(dest, NSSelectorFromString(method), destValue);
                }
            }
        }
    }
    free(properties);
}


+ (NSData *) dataForObject:(id) obj{
    if (!obj) return nil;
    return [NSKeyedArchiver archivedDataWithRootObject:obj];
}

+ (id) objcetForData:(NSData *) data
{
    if (!data) return nil;
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
}


#define  PT_KEY_VALUE @"ptkv"
+(id) doDecode:(NSData *) data{
    if(!data)
        return nil;
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *myDictionary = [unarchiver decodeObjectForKey:PT_KEY_VALUE];
    [unarchiver finishDecoding];
    return [myDictionary objectForKey:PT_KEY_VALUE];
    
}

+(NSData *) doEncode:(id)obj{
    if(!obj)
        return nil;
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:(NSMutableData *)data];
    [archiver encodeObject:[[NSDictionary alloc] initWithObjectsAndKeys:obj,PT_KEY_VALUE, nil] forKey:PT_KEY_VALUE];
    [archiver finishEncoding];
    
    return data;
}


@end


#pragma mark - AssociatValue
@implementation NSObject (AssociatValue)
- (id) associateValue:(id)value withKey:(NSString *)aKey {
    objc_setAssociatedObject( self, (__bridge void *)aKey, value, OBJC_ASSOCIATION_RETAIN);
    return self;
}

- (id) associatedValueForKey:(NSString *)aKey {
    return objc_getAssociatedObject( self, (__bridge void *)aKey );
}

#pragma mark -
static const char *__LX__Objc__ = "__LX__Objc__";

- (void) setObjc:(id)objc
{
    objc_setAssociatedObject(self, __LX__Objc__, objc, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) objc
{
    return objc_getAssociatedObject(self, __LX__Objc__);
}

@end


#pragma mark - TransformData
@implementation NSObject (TransformData)

+ (NSDictionary *) transformObjcToDic:(id) obj {
    return [NSObject transformObjcToDic:obj transients:nil];
}


+ (NSDictionary *) transformObjcToDic:(id) obj transients:(NSArray *) transients
{
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList([obj class], &outCount);
    
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:0];
    
    for(int i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *key = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
        
        BOOL isTransients = NO;
        if (transients) {
            NSPredicate *pTransients = [NSPredicate predicateWithFormat:@"SELF=%@", key];
            NSArray *results = [transients filteredArrayUsingPredicate:pTransients];
            isTransients = (results.count != 0);
        }
        
        if(![obj respondsToSelector:NSSelectorFromString(key)] || isTransients) continue;
        NSString *pType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
        
        NSString *keyValue = nil;
        if ([key isEqualToString:@"_Id"]) keyValue = @"id"; else keyValue = key;
        
        if ([pType hasPrefix:@"T@"]) {
            id value = ((id(*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
            if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSMutableString class]] ||
                [value isKindOfClass:[NSData class]] || [value isKindOfClass:[NSMutableData class]] ||
                [value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSMutableArray class]] ||
                [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] ||
                [value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSNull class]])
            {
                [result setObject:value forKey:keyValue];
            }else if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSMutableArray class]])
            {
                NSMutableArray *mTmp = value;
                if (mTmp.count > 0) {
                    if ([[mTmp firstObject] isKindOfClass:[NSString class]] || [[mTmp firstObject] isKindOfClass:[NSMutableString class]] ||
                        [[mTmp firstObject] isKindOfClass:[NSData class]] || [[mTmp firstObject] isKindOfClass:[NSMutableData class]] ||
                        [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] ||
                        [[mTmp firstObject] isKindOfClass:[NSNumber class]] || [[mTmp firstObject] isKindOfClass:[NSNull class]]) {
                        [result setObject:value forKey:keyValue];
                    }else {
                        NSMutableArray *m_Tmp = [NSMutableArray arrayWithCapacity:0];
                        for (id tmpObjc in value)
                            [m_Tmp addObject:[NSObject transformObjcToDic:tmpObjc transients:transients]];
                        [result setObject:m_Tmp forKey:keyValue];
                    }
                }else
                {
                    [result setObject:value forKey:keyValue];
                }
            }
            
            else if (!value) continue;
            else [result setObject:[NSObject transformObjcToDic:value transients:transients] forKey:key];
        }
        else if([pType hasPrefix:@"T{"]) {}
        else
        {
            pType = [pType lowercaseString];
            if ([pType hasPrefix:@"ti"] || [pType hasPrefix:@"tb"])
            {
                int value = ((int (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithInt:value] forKey:keyValue];
            }
            else if ([pType hasPrefix:@"tf"])
            {
                float value = ((float (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithFloat:value] forKey:keyValue];
            }
            else if([pType hasPrefix:@"td"])
            {
                double value = ((double (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithDouble:value] forKey:keyValue];
            }
            else if([pType hasPrefix:@"tl"] || [pType hasPrefix:@"tq"])
            {
                long long value = ((long long (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithLongLong:value] forKey:keyValue];
            }
            else if ([pType hasPrefix:@"tc"])
            {
                char value = ((char (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithChar:value] forKey:keyValue];
            }
            else if([pType hasPrefix:@"ts"])
            {
                short value = ((short (*)(id, SEL))objc_msgSend)(obj, NSSelectorFromString(key));
                [result setObject:[NSNumber numberWithShort:value] forKey:keyValue];
            }
        }
    }
    free(properties);
    return result;
}


+ (void) transformDicToObjc:(NSDictionary *) dic objcClass:(id *) obj
{
    [NSObject transformDicToObjc:dic objcClass:obj transients:nil];
}

+ (void) transformDicToObjc:(NSDictionary *) dic objcClass:(id *) obj transients:(NSArray *) transients
{
    if (![dic isKindOfClass:[NSDictionary class]] || !*obj) return;
    
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList([*obj class], &outCount);
    
    for(int i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *key = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
        NSString *method=[NSString stringWithFormat:@"set%@%@:", [[key substringToIndex:1] uppercaseString], [key substringFromIndex:1]];
        
        BOOL isTransients = NO;
        if (transients) {
            NSPredicate *pTransients = [NSPredicate predicateWithFormat:@"SELF=%@", key];
            NSArray *results = [transients filteredArrayUsingPredicate:pTransients];
            isTransients = (results.count != 0);
        }
        
        if(![*obj respondsToSelector:NSSelectorFromString(key)] || ![*obj respondsToSelector:NSSelectorFromString(method)] || isTransients) continue;
        NSString *pType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
        
        if ([key isEqualToString:@"_Id"]) key = @"id";
        
        id keyValue = nil;
        if ([pType hasPrefix:@"T@"]) {
            keyValue = [dic objectForKey:key];
//            if ([keyValue isKindOfClass:[NSNumber class]])
//                ((void(*)(id, SEL, id))objc_msgSend)(*obj, NSSelectorFromString(method), key);
//            else
            
            if (keyValue && ![keyValue isKindOfClass:[NSNull class]] && ![keyValue isKindOfClass:[NSNumber class]])
                ((void(*)(id, SEL, id))objc_msgSend)(*obj, NSSelectorFromString(method), keyValue);
        }
        else if([pType hasPrefix:@"T{"]) {}
        else
        {
            pType = [pType lowercaseString];
            if ([pType hasPrefix:@"ti"] || [pType hasPrefix:@"tb"])
            {
                keyValue = [dic objectForKey:key];
                if (keyValue && [keyValue isKindOfClass:[NSNumber class]])
                    ((void(*)(id, SEL, int))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue intValue]);
            }
            else if ([pType hasPrefix:@"tf"])
            {
                keyValue = [dic objectForKey:key];
                if (keyValue && [keyValue isKindOfClass:[NSNumber class]])
                    ((void(*)(id, SEL, float))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue floatValue]);
            }
            else if([pType hasPrefix:@"td"])
            {
                keyValue = [dic objectForKey:key];
                if (keyValue && [keyValue isKindOfClass:[NSNumber class]])
                    ((void(*)(id, SEL, double))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue doubleValue]);
            }
            else if([pType hasPrefix:@"tl"] || [pType hasPrefix:@"tq"])
            {
                keyValue = [dic objectForKey:key];
                if (keyValue && [keyValue isKindOfClass:[NSNumber class]])
                    ((void(*)(id, SEL, long long))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue longLongValue]);
            }
            else if ([pType hasPrefix:@"tc"])
            {
                keyValue = [dic objectForKey:key];
//                if (keyValue && ![keyValue isKindOfClass:[NSNull class]])
//                    ((void(*)(id, SEL, char))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue charValue]);
                // 暂时怎么做，
                if (keyValue && ![keyValue isKindOfClass:[NSNull class]])
                    ((void(*)(id, SEL, id))objc_msgSend)(*obj, NSSelectorFromString(method), keyValue);
            }
            else if([pType hasPrefix:@"ts"])
            {
                keyValue = [dic objectForKey:key];
                if (keyValue && [keyValue isKindOfClass:[NSNumber class]])
                    ((void(*)(id, SEL, short))objc_msgSend)(*obj, NSSelectorFromString(method), [keyValue shortValue]);
            }
        }
    }
    free(properties);
}


+ (NSString *) JSONWritingPrettyPrintedDictionary:(NSDictionary *) dic
{
    
    NSString *strJSON = nil;
    if ([NSJSONSerialization isValidJSONObject:dic])
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
        strJSON =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return strJSON;
}

@end



@implementation NSObject (MLResize)

+ (CGFloat) resize_scale {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    if(width > [UIScreen mainScreen].bounds.size.height)
        width = [UIScreen mainScreen].bounds.size.height;
    
    if (__DEVICE_IPAD) return 1;
    else return width / 375;
}

@end

