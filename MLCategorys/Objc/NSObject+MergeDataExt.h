//
//  NSObject+MergeDataExt.h
//

#import <Foundation/Foundation.h>

@interface NSObject (MergeDataExt)

/**
 *  @brief 把一个NSDictionary转化成对象  // 目前只支持一级字典
 *  @param dic 需要转化的对象 // 目前只支持一级字典
 *  @param cls 需要转化对象类 返回赋值后的对象
 *  @return 返回一个转化后的对象
 */
+ (id) transformDicToObjc:(NSDictionary *) dic cls:(Class) cls;

@end
