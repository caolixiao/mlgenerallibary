//
//  NSObject+MergeData.h
//

#import <Foundation/Foundation.h>

#pragma mark - MergeData
@interface NSObject (MergeData)

/**
 *  @brief 数据结构中的数据的复制
 *  @param src 原数据 需要复制的数据
 *  @param dest 目标数据  复制完成后的数据
 */
+ (void) copyObject:(id) src toDest:(id) dest;

/**
 *  @brief 数据结构中的数据的复制
 *  @param src 原数据 需要复制的数据
 *  @param dest 目标数据  复制完成后的数据
 *  @param transients 过虑字端的数组
 */
+ (void) copyObject:(id) src toDest:(id) dest transients:(NSArray *) transients;


/**
 *
 */
+ (NSData *) dataForObject:(id) obj;
+ (id) objcetForData:(NSData *) data;

+(id)doDecode:(NSData *) data;
+(NSData *) doEncode:(id)obj;

@end


#pragma mark - AssociatValue
@interface NSObject (AssociatValue)

- (id) associateValue:(id)value withKey:(NSString *)aKey;
- (id) associatedValueForKey:(NSString *)aKey;

#pragma mark -
@property (nonatomic, strong) id objc;

@end


#pragma mark - TransformData
@interface NSObject (TransformData)

/**
 *  @brief 把一个对象转化成 NSDictionary 数据
 *  @param obj 需要转化的对象
 *  @return 转化后的dic数据
 */
+ (NSDictionary *) transformObjcToDic:(id) obj;

/**
 *  @brief 把一个对象转化成 NSDictionary 数据  (如果是id 则需要通过_Id来转化)
 *  @param obj 需要转化的对象
 *  @param transients 过滤的字段
 *  @return 转化后的dic数据
 */
+ (NSDictionary *) transformObjcToDic:(id) obj transients:(NSArray *) transients;

/**
 *  @brief 把一个NSDictionary转化成对象  // 目前只支持一级字典
 *  @param dic 需要转化的对象 // 目前只支持一级字典
 *  @param obj 需要转化对象类 返回赋值后的对象
 */
+ (void) transformDicToObjc:(NSDictionary *) dic objcClass:(id *) obj;

/**
 *  @brief 把一个NSDictionary转化成对象  // 目前只支持一级字典
 *  @param dic 需要转化的对象 // 目前只支持一级字典
 *  @param obj 需要转化对象类 返回赋值后的对象
 *  @param transients 过滤的字段
 */
+ (void) transformDicToObjc:(NSDictionary *) dic objcClass:(id *) obj transients:(NSArray *) transients;


/**
 *  @brief 把一个NSDictionary对象转化成json数据
 *  @param dic 需要转化的对象
 *  @return 转化后的json数据
 */
+ (NSString *) JSONWritingPrettyPrintedDictionary:(NSDictionary *) dic;

@end


@interface NSObject (MLResize)

+ (CGFloat) resize_scale;

@end
