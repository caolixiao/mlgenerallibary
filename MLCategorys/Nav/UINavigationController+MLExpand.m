//
//  UINavigationController+MLExpand.m
//

#import "UINavigationController+MLExpand.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>

@implementation UINavigationController (MLExpand)


#pragma mark - push
- (void) pushRootViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSMutableArray *navVC = [NSMutableArray arrayWithArray:self.viewControllers];
    [navVC removeAllObjects];
    [navVC addObject:viewController];
    [self setViewControllers:navVC animated:animated];
}

- (void) pushViewController:(UIViewController *)viewController to:(Class) target animated:(BOOL)animated
{
    NSMutableArray *navVC = [NSMutableArray arrayWithArray:self.viewControllers];
    for (int i = (int)navVC.count - 1; i > 0; i--) {
        if ([[navVC lastObject] isKindOfClass:target]) break;
        [navVC removeLastObject];
    }
    
    [navVC addObject:viewController];
    [self setViewControllers:navVC animated:animated];
}

- (void) pushViewController:(UIViewController *)viewController removeCls:(Class) cls animated:(BOOL)animated
{
    NSMutableArray *navVC = [NSMutableArray arrayWithArray:self.viewControllers];
    if ([[navVC lastObject] isKindOfClass:cls] || [[navVC lastObject] isKindOfClass:cls]) [navVC removeLastObject];
    [navVC addObject:viewController];
    [self setViewControllers:navVC animated:animated];
}

- (void) pushViewControllers:(NSArray *) viewControls to:(Class) target animated:(BOOL)animated {
    NSMutableArray *navVC = [NSMutableArray arrayWithArray:self.viewControllers];
    for (int i = (int)navVC.count - 1; i > 0; i--) {
        if ([[navVC lastObject] isKindOfClass:target]) break;
        [navVC removeLastObject];
    }
    [navVC addObjectsFromArray:viewControls];
    [self setViewControllers:navVC animated:animated];
}

- (void) pushViewControllers:(NSArray *) viewControls removeCls:(Class) cls animated:(BOOL)animated {
    NSMutableArray *navVC = [NSMutableArray arrayWithArray:self.viewControllers];
    if ([[navVC lastObject] isKindOfClass:cls] || [[navVC lastObject] isKindOfClass:cls]) [navVC removeLastObject];
    [navVC addObjectsFromArray:viewControls];
    [self setViewControllers:navVC animated:animated];
}

#pragma mark - pop
- (void) popToViewControllerClass:(Class) viewController animated:(BOOL)animated
{
    for (UIViewController *vc in self.viewControllers) {
        if ([vc isKindOfClass:viewController]) {
            [self popToViewController:vc animated:animated];
            break;
        }
    }
}

- (void) pop2ViewControllerAnimated:(BOOL)animated
{
    if (self.viewControllers.count > 2)
    {
        UIViewController *vc = [self.viewControllers objectAtIndex:self.viewControllers.count - 3];
        [self popToViewController:vc animated:YES];
    }
    else
    {
        [self popViewControllerAnimated:YES];
    }
}

- (void) pop3ViewControllerAnimated:(BOOL)animated
{
    if (self.viewControllers.count > 3)
    {
        UIViewController *vc = [self.viewControllers objectAtIndex:self.viewControllers.count - 4];
        [self popToViewController:vc animated:YES];
    }
    else
    {
        [self popViewControllerAnimated:YES];
    }
}

- (BOOL) isPop:(UIViewController *) viewController {
    return ![self.viewControllers containsObject:viewController];
}

#pragma mark - InterfaceOrientations
-(BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}

#pragma mark - status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    UIViewController* topVC = self.topViewController;
    return [topVC preferredStatusBarStyle];
}

@end



#pragma mark -  UINavigationBar
@implementation UINavigationBar (MLExpand)

- (void) setWebBackground {
    UIColor *color = __RGB(0x191b21);
    self.backgroundColor = color;
    [self setBackgroundImage:[UIImage imageNavWithColor:color]];
    UIFont *font = [UIFont boldSystemFontOfBaseSize:15.0f];
    [self setTitleColor:__RGBA(0xffffff, 0.8) font:font titleShadowColor:[UIColor clearColor] shadowOffset:CGSizeZero];
    
    [self setShadowImage:[UIImage imageWithColor:color size:CGSizeMake(__SCREEN_SIZE.width, 1)]];
    
    if ([UINavigationBar instancesRespondToSelector:@selector(setShadowImage:)])
        [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:color size:CGSizeMake(__SCREEN_SIZE.width, 1)]];
}

- (void) setDefaultBackground
{
    self.backgroundColor = COLOR_GENERAL;
    [self setBackgroundImage:[UIImage imageNavWithColor:COLOR_GENERAL]];
    UIFont *font = [UIFont boldSystemFontOfBaseSize:18.0f];
    [self setTitleColor:__RGB(0xffffff) font:font titleShadowColor:[UIColor clearColor] shadowOffset:CGSizeZero];

    [self setShadowImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(__SCREEN_SIZE.width, 1)]];
    
    if ([UINavigationBar instancesRespondToSelector:@selector(setShadowImage:)])
        [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(__SCREEN_SIZE.width, 0.5)]];
}

- (void) setDefaultBackground:(UIColor *) bkg word:(UIColor *) word
{
    self.backgroundColor = bkg;
    [self setBackgroundImage:[UIImage imageNavWithColor:bkg]];
    
    UIFont *font = [UIFont boldSystemFontOfBaseSize:18.0f];
    [self setTitleColor:word font:font titleShadowColor:[UIColor clearColor] shadowOffset:CGSizeZero];
    
    [self setShadowImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(__SCREEN_SIZE.width, 1)]];
    
    if ([UINavigationBar instancesRespondToSelector:@selector(setShadowImage:)])
        [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(__SCREEN_SIZE.width, 1)]];
}


- (void) setBackgroundImage:(UIImage *)image{
    self.translucent = NO;
    
    if ([self respondsToSelector:@selector(setBackgroundImage:forBarPosition:barMetrics:)]) {
        [self setBackgroundImage:image forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    }else{
        if ([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
            [self setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        }
    }
}

- (void) setTintColor:(UIColor *)color {
    self.translucent = NO;
    if ([self respondsToSelector:@selector(barTintColor)]) self.barTintColor = color;
    else self.tintColor = color;
}

-(void)setTitleColor:(UIColor *)color{
    [self setTitleColor:color font:nil titleShadowColor:nil shadowOffset:CGSizeZero];
}

-(void)setTitleColor:(UIColor *)color font:(UIFont *)font titleShadowColor:(UIColor *)shadowColor shadowOffset:(CGSize)shadowOffset{
    //设置字体颜色
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, 0) forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTintColor:color];
    NSMutableDictionary *attributes =[[NSMutableDictionary alloc] init];
    
    if(color) [attributes setObject:color forKey:NSForegroundColorAttributeName];
    if(font) [attributes setObject:font forKey:NSFontAttributeName];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = shadowColor;
    shadow.shadowOffset = shadowOffset;
    
    [attributes setObject:shadow forKey:NSShadowAttributeName];
    
    [self setTitleTextAttributes:attributes];
}

@end




#pragma mark - UINavigationItem
NSString *const kDelegateA = @"__DelegateA";

@implementation UINavigationItem (MLExpand)

@dynamic butLeft;
@dynamic butRight;
@dynamic butRight2;

static const char *__CustomNavItem__butLeft__ = "__CustomNavItem__butLeft__";
static const char *__CustomNavItem__butRight__ = "__CustomNavItem__butRight__";
static const char *__CustomNavItem__butRight2__ = "__CustomNavItem__butRight2__";
- (void) setButLeft:(UIButton *)butLeft { objc_setAssociatedObject(self, __CustomNavItem__butLeft__, butLeft, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butLeft { return objc_getAssociatedObject(self, __CustomNavItem__butLeft__); }
- (void) setButRight:(UIButton *)butRight { objc_setAssociatedObject(self, __CustomNavItem__butRight__, butRight, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butRight { return objc_getAssociatedObject(self, __CustomNavItem__butRight__); }
- (void) setButRight2:(UIButton *)butRight2 { objc_setAssociatedObject(self, __CustomNavItem__butRight2__, butRight2, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butRight2 { return objc_getAssociatedObject(self, __CustomNavItem__butRight2__); }

#pragma mark - title
// title
- (void) addTitle:(NSString *) title
{
    [self setHidesBackButton:YES];
    [self setLeftBarButtonItem:nil];
    [self setRightBarButtonItem:nil];
    
    self.title = title;
}

- (void) modTitle:(NSString *) title {
    self.title = title;
}

- (void) setCenterView:(UIView *) view {
    self.titleView = view;
}

#pragma mark - left
- (void) setLeftView:(UIView *) view
{
    UIBarButtonItem* leftItem= [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setLeftBarButtonItem:leftItem];
}

- (void) setLeftTitle:(NSString *) title {
    [self setLeftImg:nil title:title];
}

- (void) setLeftImg:(NSString *) imgPath {
    [self setLeftImg:imgPath title:nil];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title
{
    [self setLeftImg:imgPath title:title imgType:NavItemImgTypeLeft];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor {
    [self setLeftImg:imgPath title:title font:[UIFont systemFontOfBaseSize:14] color:wColor imgType:NavItemImgTypeLeft];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type;
{
    [self setLeftImg:imgPath title:title font:[UIFont systemFontOfBaseSize:14] imgType:type];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type {
    [self setLeftImg:imgPath title:title font:font color:__RGB(0xffffff) imgType:type];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type
{
    if (!imgPath && !title) return;
    
    BOOL isImg = NO;
    self.butLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.butLeft addTarget:self action:@selector(backItemButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (imgPath && type != NavItemImgTypeNone) {
        isImg = YES;
        UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        if (!imgNormal) imgNormal = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        UIImage *imgHighlighted = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        if (!imgHighlighted) imgHighlighted = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        
        
        
        if (type == NavItemImgTypeBKG) {
            [self.butLeft setBackgroundImage:imgNormal forState:UIControlStateNormal];
            [self.butLeft setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
        }else {
            [self.butLeft setImage:imgNormal forState:UIControlStateNormal];
            [self.butLeft setImage:imgHighlighted forState:UIControlStateHighlighted];
            self.butLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    }
    
    if (title) {
        self.butLeft.titleLabel.font = font;
        [self.butLeft setTitle:title forState:UIControlStateNormal];
        [self.butLeft setTitleColor:wColor forState:UIControlStateNormal];
        [self.butLeft setTitleColor:[wColor colorWithAlphaComponent:0.4] forState:UIControlStateHighlighted];
    }
    
    {
        [self.butLeft sizeToFit];
        
        CGFloat fW = self.butLeft.bounds.size.width + CGScale(5);
        CGFloat fH = self.butLeft.bounds.size.height;
        if (fW > 120) fW = 120;
        if (fH > CGScale(40)) fH = CGScale(40);
        self.butLeft.frame = (CGRect){CGPointZero, {fW, fH}};
        
        if (isImg && title) {
            CGFloat fW_img = self.butLeft.imageView.bounds.size.width;
            CGFloat fH_img = self.butLeft.imageView.bounds.size.height;
            if (type == NavItemImgTypeLeft) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(2, 0, 2, fW - fW_img - 4)];
            }else if (type == NavItemImgTypeRight) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fW_img)];
                [self.butLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeUp) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fH_img)];
                [self.butLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeDown) {
                
            }
        }
    }

    UIBarButtonItem* backItem= [[UIBarButtonItem alloc] initWithCustomView:self.butLeft];
    [self setLeftBarButtonItem:backItem];
}

#pragma mark - right
- (void) setRightView:(UIView *) view
{
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setRightBarButtonItem:rightItem];
}

- (void) setRightTitle:(NSString *) title {
    [self setRightImg:nil title:title];
}

- (void) setRightImg:(NSString *) imgPath {
    [self setRightImg:imgPath title:nil];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title {
    [self setRightImg:imgPath title:title imgType:NavItemImgTypeLeft];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor {
    [self setRightImg:imgPath title:title font:[UIFont systemFontOfBaseSize:14] color:wColor imgType:NavItemImgTypeLeft];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type {
    [self setRightImg:imgPath title:title font:[UIFont systemFontOfBaseSize:14] imgType:type];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type {
    [self setRightImg:imgPath title:title font:font color:__RGB(0xffffff) imgType:type];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type
{
    if (!imgPath && !title) return;
    
    BOOL isImg = NO;
    self.butRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.butRight addTarget:self action:@selector(rightItemButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (imgPath && type != NavItemImgTypeNone) {
        isImg = YES;
        UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        if (!imgNormal) imgNormal = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        UIImage *imgHighlighted = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        if (!imgHighlighted) imgHighlighted = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        if (!imgHighlighted && imgNormal) imgHighlighted = [imgNormal imageWithAlpha:0.6];
        
        if (type == NavItemImgTypeBKG || type == NavItemImgTypePoint) {
            [self.butRight setBackgroundImage:imgNormal forState:UIControlStateNormal];
            [self.butRight setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
        }else {
            [self.butRight setImage:imgNormal forState:UIControlStateNormal];
            [self.butRight setImage:imgHighlighted forState:UIControlStateHighlighted];
        }
    }
    
    if (title) {
        self.butRight.titleLabel.font = font;
        [self.butRight setTitle:title forState:UIControlStateNormal];
        [self.butRight setTitleColor:wColor forState:UIControlStateNormal];
        [self.butRight setTitleColor:[wColor colorWithAlphaComponent:0.4] forState:UIControlStateHighlighted];
    }
    
    {
        [self.butRight sizeToFit];
        
        CGFloat fW = self.butRight.bounds.size.width;
        CGFloat fH = self.butRight.bounds.size.height;
        if (fW > 120) fW = 120;
        if (fH > CGScale(40)) fH = CGScale(40);
        self.butRight.frame = (CGRect){CGPointZero, {fW, fH}};
        
        if (isImg && title) {
            CGFloat fW_img = self.butRight.imageView.bounds.size.width;
            CGFloat fH_img = self.butRight.imageView.bounds.size.height;
            if (type == NavItemImgTypeLeft) {
                
            }else if (type == NavItemImgTypeRight) {
                [self.butRight setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fW_img)];
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeUp) {
                [self.butRight setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fH_img)];
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeDown) {
                
            }else if (type == NavItemImgTypePoint) {
                [self.butRight setTitle:[NSString stringWithFormat:@" %@ ", title] forState:UIControlStateNormal];
                self.butRight.titleLabel.backgroundColor = [UIColor redColor];
                self.butRight.titleLabel.layer.cornerRadius = CGScale(6);
                self.butRight.titleLabel.layer.masksToBounds = YES;
                CGFloat tmp = VIEWW(self.butRight.titleLabel)/2.0 + 5;
                CGFloat fL = VIEWW(self.butRight)/2.0 + tmp - 10;
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(-6, fL, 12, -tmp)];
            }
        }
    }
    
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:self.butRight];
    [self setRightBarButtonItem:rightItem];
}

- (void) setRight1Img:(NSString *) img1 right2:(NSString *) img2
{
    UIView *view = [[UIView alloc] init];
    
    self.butRight = [UIButton buttonWithType:UIButtonTypeCustom];
    self.butRight.translatesAutoresizingMaskIntoConstraints = NO;
    [self.butRight addTarget:self action:@selector(centerItemButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *img1Normal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",img1]];
    [self.butRight setImage:img1Normal forState:UIControlStateNormal];
    [self.butRight setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img1]] forState:UIControlStateHighlighted];
    [self.butRight setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img1]] forState:UIControlStateSelected];
    [view addSubview:self.butRight];
    
    self.butRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.butRight2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.butRight2 addTarget:self action:@selector(rightItemButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",img2]];
    [self.butRight2 setImage:imgNormal forState:UIControlStateNormal];
    [self.butRight2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img2]] forState:UIControlStateHighlighted];
    [self.butRight2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img2]] forState:UIControlStateSelected];
    [view addSubview:self.butRight2];
    
    view.frame = CGRectMake(0.0f, 0.0f, img1Normal.size.width + imgNormal.size.width + 20.0f, img1Normal.size.height + 3.0f);
    
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setRightBarButtonItem:rightItem];
    
}

#pragma mark - Methon
//返回上进入的视图
- (void) backItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(backItemButton:)])
        [delegate backItemButton:button];
    else
        NSLog(@"需要重构 - (void) backItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) rightItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(rightItemButton:)])
        [delegate rightItemButton:button];
    else
        NSLog(@"需要重构 - (void) rightItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) centerItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(centerItemButton:)])
        [delegate centerItemButton:button];
    else
        NSLog(@"需要重构 - (void) centerItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) setDelegate:(id)delegate
{
    objc_setAssociatedObject( self, (__bridge void *)kDelegateA, delegate, OBJC_ASSOCIATION_ASSIGN);
}

@end
