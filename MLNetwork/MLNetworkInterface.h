//
//  MLNetworkInterface.h
//

#import <Foundation/Foundation.h>

/** ************************************** 宏 begin ************************************** **/
#define NET_SUCCESS             0
#define NET_OTHER_FAIL          5000
#define NET_JSON_FAIL           5001
#define NET_FUZHI_FAIL          5002


typedef void(^finsh) (id objc);
typedef void(^complate) (NSObject *objc, NSArray *bModel);

#pragma mark -
@interface MLNetworkInterface : NSObject

+ (instancetype) shareNetInterface;

/**
 *  @brief 获取股票详情
 *  @param codes 股票代码列表
 *  @param finsh 返回的数据结构
 */
- (void) loadAllDescWithCodes:(NSArray *) codes complate:(complate) finsh;

@end
