//
//  MLNetworkInterface.m
//

#import "MLNetworkInterface.h"
#import "NSString+MLCustom.h"


@interface NSArray (MLDouble)

- (NSNumber *) doubNum:(int) index;

@end

@implementation NSArray (MLDouble)

- (NSNumber *) doubNum:(int) index {
    return @([self[index] doubleValue]);
}

@end

@interface MLNetworkInterface ()

typedef void analysis(NSData *data, NSHTTPURLResponse *resp, NSDictionary **_dict);
void analysisData(NSData *data, NSHTTPURLResponse *resp, NSError *error, NSDictionary **_dict, analysis mt = nil);

@end

@implementation MLNetworkInterface

void analysisData(NSData *data, NSHTTPURLResponse *resp, NSError *error, NSDictionary **_dict, analysis mt) {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
    if (data && resp) {
        INFO_NSLOG(@"网络获取的数据为： statusCode = %ld data=%@", (long)resp.statusCode,  [[NSMutableString  alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        if (resp.statusCode == 200)
        {
            
            if (mt) {
                mt(data, resp, _dict);
                return ;
            }
            else {
                NSString *contentType = [resp.allHeaderFields objectForKey:@"Content-Type"];
                if (contentType && [contentType rangeOfString:@"application/json"].location != NSNotFound)
                {
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
                    
                    if (!json)
                    {
                        
                        INFO_NSLOG(@"网络获取的数据解析json失败");
                        
                        dict[@"status"] = @(NET_JSON_FAIL);
                        dict[@"msg"] = @"解析json失败";
                        
                    }
                    else
                    {
                        dict[@"status"] = @(NET_SUCCESS);
                        dict[@"msg"] = @"";
                        dict[@"data"] = json;
                    }
                    
                }
            }
        }
        else
        {
            
            dict[@"status"] = @(resp.statusCode);
            dict[@"msg"] = [error.userInfo description];
            
        }
        
    }
    else if (error)
    {
        
        dict[@"status"] = @(error.code);
        if ([error.userInfo objectForKey:@"NSLocalizedDescription"]) dict[@"msg"] = [error.userInfo objectForKey:@"NSLocalizedDescription"];
        else dict[@"msg"] = error.domain;
        
    }
    else
    {
        
        dict[@"status"] = @(NET_OTHER_FAIL);
        dict[@"msg"] = @"未知错误";
    }
    
    *_dict = dict;
}


#pragma mark -
void analysisSinaDetail(NSData *data, NSHTTPURLResponse *resp, NSDictionary **_dict) {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
    NSString *dateStr = resp.allHeaderFields[@"date"];
    dict[@"date"] = [PubGlobal dateForServerDate:dateStr];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString *str  = [[NSMutableString  alloc] initWithData:data encoding:gbkEncoding];
    INFO_NSLOG(@"网络获取的数据为： statusCode = %ld data=%@", (long)resp.statusCode,  str);
    NSArray *m_ATmp = [str componentsSeparatedByString:@";\n"];
    
//    if (m_ADict.count != 0) {
//        dict[@"status"] = @(NET_SUCCESS);
//        dict[@"msg"] = @"";
//        dict[@"data"] = m_ADict;
//    }else {
//        dict[@"status"] = @(NET_JSON_FAIL);
//        dict[@"msg"] = @"解析json失败";
//    }
    
    *_dict = dict;
}


#pragma mark -
+ (instancetype) shareNetInterface
{
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

#pragma mark - NTES
/**
 *  @brief 获取股票详情
 *  @param codes 股票代码列表
 *  @param finsh 返回的数据结构
 */
- (void) loadAllDescWithCodes:(NSArray *) codes complate:(complate) finsh {
    
//    NSString *urlPath = [kBase_PROTOCOL_URL stringByAppendingFormat:kURLStringAllFeeds, [array componentsJoinedByString:@","]];
//    INFO_NSLOG(@">>>>>>>>>>>>>>>>>>> %@", urlPath);
//    [[[NSURLSession sharedSession] dataTaskWithRequest:[PubGlobal setHeadInfoURLPath:urlPath] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        
//        NSDictionary *dict = nil;
//        analysisData(data, (NSHTTPURLResponse *)response, error, &dict, analysisDetail);
//
//        LSBaseModel *base = [[LSBaseModel alloc] init];
//        [NSObject transformDicToObjc:dict objcClass:&base];
//        
//            for (NSString *code in array) {
//                NSDictionary *tmp = dict[code];
//                if (!tmp) continue;
//                
//                LSDetailsModel *model = [[LSDetailsModel alloc] init];
//                [NSObject transformDicToObjc:tmp objcClass:&model];
//                model.time = [PubGlobal timestampForString:tmp[@"time"]];
//                model.percent = model.percent * 100;
//                model.code = [model.code substringFromIndex:1];
//                [base.list addObject:model];
//        
//            base.status = NET_FUZHI_FAIL;
//            base.msg = @"数据解析错误";
//        }
//        base.data = nil;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            finsh(base, m_ATmp);
//        });
//        
//    }] resume];
}


@end
