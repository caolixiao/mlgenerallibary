//
//  MLConstantValue.h
//

#ifndef MLConstantValue_h
#define MLConstantValue_h

/****************************************  界面颜色值 begin  ****************************************/

extern UIColor *COLOR_GENERAL;

extern UIColor *COLOR_LINE;

extern UIColor *COLOR_WORD;
extern UIColor *COLOR_WORD_2;

extern UIColor *COLOR_BKG;
extern UIColor *COLOR_BKG_2;


void setCOLOR_GENERAL(UIColor *value);
void setCOLOR_LINE(UIColor *value);
void setCOLOR_WORD(UIColor *value);
void setCOLOR_WORD_2(UIColor *value);
void setCOLOR_BKG(UIColor *value);
void setCOLOR_BKG_2(UIColor *value);


/****************************************  界面颜色值 end ****************************************/


/****************************************  界面所有文字的大小 begin  ****************************************/

// 字体尽可能要用这里边的 除非特别需要我们在添加
#define WORD_SIZE_GENERAL       (14.0f)// 通用的字体
#define WORD_SIZE_TITLE         (15.0f)// 设置标题的字体大小
#define WORD_SIZE_CONTENTS      (13.0f)// 设置内容的字体大小
#define WORD_SIZE18             (18.0f)
#define WORD_SIZE12             (12.0f)
#define WORD_SIZE11             (11.0f)
#define WORD_SIZE10             (10.0f)
#define WORD_SIZE8              (8.0f)

#define FONT_SIZE_GENERAL        [UIFont systemFontOfBaseSize:WORD_SIZE_GENERAL]// 通用的字体
#define FONT_SIZE_TITLE          [UIFont systemFontOfBaseSize:WORD_SIZE_TITLE]// 设置标题的字体大小
#define FONT_SIZE_CONTENTS       [UIFont systemFontOfBaseSize:WORD_SIZE_CONTENTS]// 设置内容的字体大小
#define FONT_SIZE18              [UIFont systemFontOfBaseSize:WORD_SIZE18]// 设置时间的字体大小
#define FONT_SIZE12              [UIFont systemFontOfBaseSize:WORD_SIZE12]// 设置时间的字体大小
#define FONT_SIZE11              [UIFont systemFontOfBaseSize:WORD_SIZE11]// 设置时间的字体大小
#define FONT_SIZE10              [UIFont systemFontOfBaseSize:WORD_SIZE10]// 设置时间的字体大小
#define FONT_SIZE8               [UIFont systemFontOfBaseSize:WORD_SIZE8]// 设置时间的字体大小


/****************************************  界面所有文字的大小 end ****************************************/


/****************************************  比例值 begin  ****************************************/

//
#define CGScale_4_3       (4.0/3.0)
#define CGScale_3_4       (3.0/4.0)

/****************************************  比例值 end ****************************************/


/****************************************  高度值 begin  ****************************************/

#define GET_MIN_FALOAT              (0.000001)
#define GET_FEdge                   CGScale(14.0)   //

#define GET_DEFAULT_CELL_HEIGHT     CGScale(44.0)   // 默认cell的高度
#define GET_BUTTON_HEIGHT           CGScale(36.0)   // 大按钮的高度
#define GET_TEXTFIELD_HEIGT         CGScale(40.0)   // 输入框的高度
#define GET_SECTION_HEIGT           CGScale(18.0)   // 内容块的间隙高度


/****************************************  高度值 end ****************************************/


#endif
