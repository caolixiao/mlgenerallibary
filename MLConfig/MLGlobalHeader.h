//
//  MLGlobalHeader.h
//

#ifndef MLGlobalHeader_h
#define MLGlobalHeader_h

#define __isMLLib__

#import <Foundation/Foundation.h>

#import "MLDeviceConfig.h"
#import "MLGlobalConfig.h"
#import "MLConstantValue.h"

#import "NSObject+MergeData.h"
#import "NSObject+MergeDataExt.h"

#import "UIView+MLCustom.h"
#import "UIImage+MLCustom.h"
#import "UIFont+MLResize.h"

#import "UINavigationController+MLExpand.h"

// view
#ifndef __isMLLib__
    #import "MLMessageTool.h"
#endif

#endif
