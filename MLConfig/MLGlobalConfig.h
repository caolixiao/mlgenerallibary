//
//  GlobalConfig.h
//

#ifndef GlobalConfig_h
#define GlobalConfig_h

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

// 获取版本
#define VERSION [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"]
#define BUILDVERSION [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleVersion"]
#define BUNDLEIDENTIFIER [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleIdentifier"]
#define APPNAME [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleExecutable"]
#define ICONPATH [[[NSBundle mainBundle].infoDictionary valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject]
#define IMGICON [UIImage imageNamed:ICONPATH]

#define VERSION_STR [VERSION stringByReplacingOccurrencesOfString:@"." withString:@""]
#define VERSION_INT [[VERSION stringByReplacingOccurrencesOfString:@"." withString:@""] intValue]

#define CHANNELID (DEBUG ? @"dev" : @"app store")
#define CHANNELID(__dev__) (__dev__ ? @"dev" : @"app store")

//
#define __OrientationIsPortrait UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation)
#define __OrientationIsLandscape UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)


// screen
#define __SCREEN_BOUNDS             [[UIScreen mainScreen] bounds]
#define __SCREEN_SIZE               [UIScreen mainScreen].bounds.size
#define __SCREEN_BOUNDS_SIZE_CHAGE  CGRectMake(0.0f, 0.0f, __SCREEN_SIZE.height, __SCREEN_SIZE.width)
#define __RECT_SIZE_CHAGE(__rect)   CGRectMake(__rect.origin.x, __rect.origin.y, __rect.size.height, __rect.size.width)
#define __WINDEOW_BOUNDS            [[UIApplication sharedApplication].delegate window].bounds //只能在定义window后用

#define CGScale(__v__)              ceil(((__v__)*(__SCREEN_SIZE.width/375.0)))
#define CGScaleSize(__w__, __h__)   CGSizeMake(CGScale(__w__), CGScale(__h__))
#define CGScaleRect(__x__, __y__, __w__, __h__)     CGRectMake(CGScale(__x__), CGScale(__y__), CGScale(__w__), CGScale(__h__))
#define CGScaleRectXY(__x__, __y__, __w__, __h__)   CGRectMake(CGScale(__x__), CGScale(__y__), __w__, __h__)
#define CGScaleRectX(__x__, __y__, __w__, __h__)    CGRectMake(CGScale(__x__), __y__, __w__, __h__)
#define CGScaleRectY(__x__, __y__, __w__, __h__)    CGRectMake(__x__, CGScale(__y__), __w__, __h__)
#define CGScaleRectSize(__x__, __y__, __w__, __h__) CGRectMake(__x__, __y__, CGScale(__w__), CGScale(__h__))
#define CGScale_IPad(__v__)         (__DEVICE_IPAD ? ceil((__v__)*(768/320.0)) : ceil(((__v__)*(__SCREEN_SIZE.width/375.0))))

//
#define VIEWX(__view)               (__view.frame.origin.x)
#define VIEWY(__view)               (__view.frame.origin.y)
#define VIEWW(__view)               (__view.frame.size.width)
#define VIEWH(__view)               (__view.frame.size.height)
#define VIEWMaxX(__view)            (__view.frame.origin.x + __view.frame.size.width)
#define VIEWMaxY(__view)            (__view.frame.origin.y + __view.frame.size.height)

// color
#define __RGBA(__rgb, __a) [UIColor colorWithRed:((float)((__rgb & 0xFF0000) >> 16))/255.0 green:((float)((__rgb & 0xFF00) >> 8))/255.0 blue:((float)(__rgb & 0xFF))/255.0 alpha:__a]

#define __RGB(__rgb) [UIColor colorWithRed:((float)((__rgb & 0xFF0000) >> 16))/255.0 green:((float)((__rgb & 0xFF00) >> 8))/255.0 blue:((float)(__rgb & 0xFF))/255.0 alpha:1.0f]


// release view
#define SAFERELEASEVIEW(__v__) if (__v__ && __v__.superview) [__v__ removeFromSuperview]; __v__ = nil;


//
#define __ROUND(__d__, __len__, __out__) \
do { \
    long tmp = 1;   \
    for (int i = 0; i < __len__; i++) tmp = tmp*10; \
    if (__d__ > 0) *__out__ = (int)((__d__ * tmp) + 0.5)/100.0; \
    else *__out__ = (int)((__d__ * tmp) - 0.5)/100.0; \
} while (0)


// waring
//#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"     Stuff;

#define PerformSelectorWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if ([self respondsToSelector:NSSelectorFromString(Stuff)]) \
[self performSelector:NSSelectorFromString(Stuff) withObject:nil];   \
_Pragma("clang diagnostic pop") \
} while (0)


#define PerformSelectorWarningTargetWithSel(__target, Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if (__target && [__target respondsToSelector:Stuff]) \
[__target performSelector:Stuff withObject:nil]; \
_Pragma("clang diagnostic pop") \
} while (0)

#define PerformSelectorWarningTarget(__target, __action) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if (__target && [__target respondsToSelector:NSSelectorFromString(__action)]) \
[__target performSelector:NSSelectorFromString(__action) withObject:nil withObject:nil];   \
_Pragma("clang diagnostic pop") \
} while (0)


#define PerformSelectorWarningTargetWithSelAtArgs(__target, Stuff, args, args2) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if (__target && [__target respondsToSelector:Stuff]) \
[__target performSelector:Stuff withObject:args withObject:args2];   \
_Pragma("clang diagnostic pop") \
} while (0)


#define PerformSelectorWarningTargetWithArgs(__target, Stuff, args, args2) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if (__target && [__target respondsToSelector:NSSelectorFromString(Stuff)]) \
[__target performSelector:NSSelectorFromString(Stuff) withObject:args withObject:args2];   \
_Pragma("clang diagnostic pop") \
} while (0)


#define PerformSelectorWarningWithBlack(Stuff, black) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if ([self respondsToSelector:NSSelectorFromString(Stuff)]) \
black = [self performSelector:NSSelectorFromString(Stuff) withObject:nil];   \
_Pragma("clang diagnostic pop") \
} while (0);

#define PerformSelectorWarningTargetWithArgsOfBlack(__target, __action, __args, __black) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
_Pragma("clang diagnostic ignored \"-Wobjc-protocol-method-implementation\"") \
if (__target && [__target respondsToSelector:NSSelectorFromString(__action)]) \
__black = [__target performSelector:NSSelectorFromString(__action) withObject:__args];   \
_Pragma("clang diagnostic pop") \
} while (0);



// bak
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

#pragma clang diagnostic pop


/// log print
#if DEBUG

#define ERROR_NSLOG(fmt, ...) NSLog((fmt), ##__VA_ARGS__);
#define INFO_NSLOG(...)  NSLog(__VA_ARGS__);

#else

#define ERROR_NSLOG(fmt, ...)
#define INFO_NSLOG(...)

#endif


#endif
