//
//  MLDeviceConfig.h
//

#ifndef MLDeviceConfig_h
#define MLDeviceConfig_h

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

// device
#define __IS_DEVICE_SCREEN(__size) ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? \
                                    CGSizeEqualToSize(__size, [[UIScreen mainScreen] currentMode].size) : NO)

#define __DEVICE_IPHONE_PLUS        (__IS_DEVICE_SCREEN(CGSizeMake(1242, 2208)) ? YES : __IS_DEVICE_SCREEN(CGSizeMake(1125, 2001)))
#define __DEVICE_IPHONE_MAIN        __IS_DEVICE_SCREEN(CGSizeMake(750,  1334))
#define __DEVICE_IPHONE_5X          __IS_DEVICE_SCREEN(CGSizeMake(640,  1136))
#define __DEVICE_IPHONE_4S_LAST     (__IS_DEVICE_SCREEN(CGSizeMake(640,  960)) ? YES : __IS_DEVICE_SCREEN(CGSizeMake(320, 480)))
#define __DEVICE_IPAD_FROM_IPHONE   ((__IS_DEVICE_SCREEN(CGSizeMake(768, 1024)) || __IS_DEVICE_SCREEN(CGSizeMake(1024, 768))) ? \
                                    YES : ( __IS_DEVICE_SCREEN(CGSizeMake(1536, 2048)) ||  __IS_DEVICE_SCREEN(CGSizeMake(2048, 1536))))
#define __DEVICE_IPAD               (__DEVICE_IPAD_FROM_IPHONE && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))

//
#define __DEVICE_IPHONE_PLUS_SIZE       CGSizeMake(414, 736)
#define __DEVICE_IPHONE_MAIN_SIZE       CGSizeMake(375, 667)
#define __DEVICE_IPHONE_5X_SIZE         CGSizeMake(320, 568)
#define __DEVICE_IPHONE_4S_LAST_SIZE    CGSizeMake(320, 480)
#define __DEVICE_IPAD_SIZE              CGSizeMake(768, 1024)


// version
//当前系统版本号
#define __SYSTEMP_OS_VERSION (int)([[UIDevice currentDevice] systemVersion].floatValue * 10000.0f)

#define __IPHONE_OS_VERSION_10X         (__SYSTEMP_OS_VERSION >= __IPHONE_10_0)
#define __IPHONE_OS_VERSION_9X          (__SYSTEMP_OS_VERSION < __IPHONE_10_0 && __SYSTEMP_OS_VERSION >= __IPHONE_9_0)
#define __IPHONE_OS_VERSION_9X_LAST     (__SYSTEMP_OS_VERSION < __IPHONE_10_0)
#define __IPHONE_OS_VERSION_8X          (__SYSTEMP_OS_VERSION < __IPHONE_9_0 && __SYSTEMP_OS_VERSION >= __IPHONE_8_0)
#define __IPHONE_OS_VERSION_8X_LAST     (__SYSTEMP_OS_VERSION < __IPHONE_9_0)
#define __IPHONE_OS_VERSION_7X          (__SYSTEMP_OS_VERSION < __IPHONE_8_0 && __SYSTEMP_OS_VERSION >= __IPHONE_7_0)
#define __IPHONE_OS_VERSION_7X_LAST     (__SYSTEMP_OS_VERSION < __IPHONE_8_0)
#define __IPHONE_OS_VERSION_6X          (__SYSTEMP_OS_VERSION < __IPHONE_7_0 && __SYSTEMP_OS_VERSION >= __IPHONE_6_0)
#define __IPHONE_OS_VERSION_6X_LAST     (__SYSTEMP_OS_VERSION < __IPHONE_7_0)

#endif
