//
//  MLConstantValue.m
//

#import "MLConstantValue.h"
#import "MLGlobalHeader.h"



/****************************************  界面颜色值 begin  ****************************************/

UIColor *COLOR_GENERAL = __RGB(0xd94b25);
UIColor *COLOR_LINE = __RGB(0xd6d6d6);
UIColor *COLOR_WORD = __RGBA(0x000000, 0.9);
UIColor *COLOR_WORD_2 = __RGBA(0x000000, 0.4);
UIColor *COLOR_BKG = __RGB(0xeeeeee);
UIColor *COLOR_BKG_2 = __RGB(0xf5f5f5);

void setCOLOR_GENERAL(UIColor *value) {
    COLOR_GENERAL = value;
}

void setCOLOR_LINE(UIColor *value) {
    COLOR_LINE = value;
}

void setCOLOR_WORD(UIColor *value) {
    COLOR_WORD = value;
}
void setCOLOR_WORD_2(UIColor *value) {
    COLOR_WORD_2 = value;
}

void setCOLOR_BKG(UIColor *value) {
    COLOR_BKG = value;
}

void setCOLOR_BKG_2(UIColor *value) {
    COLOR_BKG_2 = value;
}

/****************************************  界面颜色值 end ****************************************/
