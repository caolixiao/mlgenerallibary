//
//  MLVersionUpload.m
//

#import "MLVersionUpload.h"

@implementation MLVersionUpload


+ (instancetype) sharedInstance
{
    static MLVersionUpload *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [MLVersionUpload new];
    });
    
    return sharedInstance;
}

+ (void) checkUpBuildVersion:(UIViewController *) vc {
    [MLVersionUpload checkUpBuildVersion:vc firToken:@"d459400ee47dada9c8c0e8f78c31b8d9"];
}

+ (void) checkUpBuildVersion:(UIViewController *) vc firToken:(NSString *) token {
    NSString *urlPath = [NSString stringWithFormat:@"http://api.fir.im/apps/latest/%@?api_token=%@&type=ios", BUNDLEIDENTIFIER, token];
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlPath] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!data) return ;
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
        
        MLVersionUpload *version = [MLVersionUpload sharedInstance];
        [version processDictionary:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (version.build && ![version.build isEqualToString:BUILDVERSION] && ![BUILDVERSION hasSuffix:@".0"]) {
                
                NSString *title = [NSString stringWithFormat:@"新版本%@", version.build];
                NSString *msg = [NSString stringWithFormat:@"%@", version.changelog];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:NULL];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:version.update_url]]) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:version.update_url]];
                    }else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:version.install_url]]) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:version.install_url]];
                    } else {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://fir.im"]];
                    }
                }];
                
                [alert addAction:cancelAction];
                [alert addAction:okAction];
                
                [vc presentViewController:alert animated:YES completion:nil];
            }
        });
    }] resume];
}

-(void)processDictionary:(id)reusltObjc{
    if ([reusltObjc isKindOfClass:[NSDictionary class]]) {
        _build = [reusltObjc objectForKey:@"build"];
        _version = [reusltObjc objectForKey:@"version"];
        _changelog = [reusltObjc objectForKey:@"changelog"];
        _install_url = [reusltObjc objectForKey:@"install_url"];
        _update_url = [reusltObjc objectForKey:@"update_url"];
    }
}


@end
