//
//  MLVersionUpload.h
//


#import <Foundation/Foundation.h>

@interface MLVersionUpload : NSObject

@property (nonatomic, strong) NSString *build;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSString *changelog;
@property (nonatomic, strong) NSString *install_url;
@property (nonatomic, strong) NSString *update_url;

+ (instancetype) sharedInstance;

+ (void) checkUpBuildVersion:(UIViewController *) vc ;
+ (void) checkUpBuildVersion:(UIViewController *) vc firToken:(NSString *) token;

@end
