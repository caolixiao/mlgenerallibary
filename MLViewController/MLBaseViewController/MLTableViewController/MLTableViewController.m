//
//  MLTableViewController.m
//

#import "MLTableViewController.h"

#ifdef MLISLOADDZNEmptyDataSet
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#endif

#ifdef MLISLOADMJREFRESH
#import <MJRefresh/MJRefresh.h>
#endif

#ifdef MLISLOADDZNEmptyDataSet
@interface MLTableViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
#else
@interface MLTableViewController ()
#endif


@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation MLTableViewController

#pragma clang diagnostic pop

@synthesize tableView = _tableView;

@synthesize m_AData;
@synthesize page;

- (void) dealloc {
    INFO_NSLOG(@"dealloc MLTableViewController");
}

- (instancetype) init {
    if (self = [super init]) {
        m_AData = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    // init void data
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:[self tableViewStyle]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor =  COLOR_BKG;
        _tableView.separatorColor = COLOR_LINE;
        _tableView.dataSource = self;
        _tableView.delegate  =self;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kIdentify_Default_cell];
        
#ifdef MLISLOADDZNEmptyDataSet
        [self loadNULLDataView];
#endif
    }
}

- (UITableViewStyle)tableViewStyle{
    return UITableViewStylePlain;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier:kIdentify_Default_cell];
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return GET_MIN_FALOAT;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return GET_MIN_FALOAT;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    static NSString *CellIdentifier = @"BaseFooterIdentifier";
//    UITableViewHeaderFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:CellIdentifier];
//    if (!view) view = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:CellIdentifier];
//    view.contentView.backgroundColor = _tableView.backgroundColor;
//    return view;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"BaseHeaderIdentifier";
    UITableViewHeaderFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:CellIdentifier];
    if (!view) view = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:CellIdentifier];
    view.contentView.backgroundColor = _tableView.backgroundColor;
    return view;
}


#ifdef MLISLOADDZNEmptyDataSet
#pragma mark - DZNEmptyDataSetSource
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f], NSForegroundColorAttributeName: COLOR_GENERAL};
    return [[NSAttributedString alloc] initWithString:@"无数据"  attributes:attribute];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIColor clearColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return -VIEWH(_tableView)/2.0f +100;
}

#pragma mark - DZNEmptyDataSetDelegate
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}


#pragma mark - set is show null view
- (void) loadNULLDataView {
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
}

- (void) removeNULLDataView {
    _tableView.emptyDataSetSource = nil;
    _tableView.emptyDataSetDelegate = nil;
}
#endif


#ifdef MLISLOADMJREFRESH

#pragma mark -
#pragma mark - data refresh
- (void) refreshingView {
    page = 0;
    isNotMore = false;
    [m_AData removeAllObjects];
    
    if ([self respondsToSelector:@selector(listViewController:loadDataAtPage:)])
        [self listViewController:self loadDataAtPage:page];
    
    if([_refreshDelegate respondsToSelector:@selector(listViewController:loadDataAtPage:)])
        [_refreshDelegate listViewController:self loadDataAtPage:page];
    else if (isSuper)
        [self endRefreshing];
}

- (void) moreingView {
    if (isNotMore) {
        [self endRefreshing];
    }
    else {
        page = (page == 0) ? 1 : (page + 1);
        
        if ([self respondsToSelector:@selector(listViewController:loadDataAtPage:)])
            [self listViewController:self loadDataAtPage:page];
        
        if([_refreshDelegate respondsToSelector:@selector(listViewController:loadDataAtPage:)])
            [_refreshDelegate listViewController:self loadDataAtPage:page];
        else if (isSuper)
            [self endRefreshing];
    }
}

- (void) loadRefreshView {
    __weak typeof(self) weakSelf = self;
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshingView];
    }];
    
    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf moreingView];
    }];
    [_tableView.mj_footer endRefreshingWithNoMoreData];
}

#pragma mark - reload view
- (void) loadGetData {
    if ([self respondsToSelector:@selector(reloadData)]) [self reloadData];
    BOOL isHiddenMore = m_AData.count == 0 ? true : false;
    isNotMore = isHiddenMore;
    
    if ([self respondsToSelector:@selector(reloadDataWithTotal:num:)])
        isNotMore = [self reloadDataWithTotal:100 num:page];
    
    if ([self respondsToSelector:@selector(reloadMoreData)])
        isNotMore = ![self reloadMoreData];
    
    if (!isHiddenMore) {
        if (!isNotMore) [_tableView.mj_footer resetNoMoreData];
        else [_tableView.mj_footer endRefreshingWithNoMoreData];
    }else {
         [_tableView.mj_footer resetNoMoreData];
    }
    
    _tableView.mj_footer.automaticallyHidden = isHiddenMore;
    
    [_tableView reloadData];
    [self endRefreshing];
    if ([self respondsToSelector:@selector(reloadLastData)]) [self reloadLastData];
}

- (void) listViewController:(id) target loadDataAtPage:(int) page {
    // MARK: 子类覆盖
    isSuper = YES;
}

#pragma mark -
- (void) beginRefreshing {
    if(!_tableView.mj_header.isRefreshing)
        [_tableView.mj_header beginRefreshing];
}

- (void) endRefreshing {
    if(_tableView.mj_header && _tableView.mj_header.isRefreshing)
        [_tableView.mj_header endRefreshing];
    
    if (_tableView.mj_footer && [_tableView.mj_footer isRefreshing])
        [_tableView.mj_footer endRefreshing];
}

- (BOOL) isLoadingData {
    return (_tableView.mj_header && _tableView.mj_header.isRefreshing);
}

#endif


@end
