//
//  MLBaseViewController.m
//

#import "MLBaseViewController.h"
#import "UINavigationController+MLExpand.h"

@implementation MLBaseViewController

- (void) dealloc
{
    INFO_NSLOG(@"dealloc MLBaseViewController");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype) init {
    if (self = [super init]) {
        
        [self.navigationController.navigationBar setDefaultBackground];
        [self.navigationItem setDelegate:self];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = COLOR_BKG;
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void) addTitle:(NSString *) title {
    [self.navigationItem addTitle:title];
}

- (void) setShowTitle:(NSString *) title {
    [self.navigationItem modTitle:title];
}

- (void) addleftAndTitle:(NSString *) title
{
    [self.navigationItem modTitle:title];
    [self.navigationItem setLeftImg:@"nav_back" title:nil color:COLOR_WORD];
}

- (void) addLeftV:(UIView *) view {
    [self.navigationItem setLeftView:view];
}

- (void) addRightV:(UIView *) view
{
    [self.navigationItem addTitle:nil];
    [self.navigationItem setRightView:view];
}

-(void)addCenterV:(UIView *)view{
    [self.navigationItem setLeftImg:@"nav_back" title:nil];
    self.navigationItem.titleView = view;
}


#pragma mark - super action
- (void) backItemButton:(UIButton *) button
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - set child vc
- (UIView *) contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.frame = self.view.bounds;
        [_contentView setBackgroundColor:[UIColor clearColor]];
        [_contentView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    }
    return _contentView;
}

#pragma mark - view controller
- (UIViewController *)selectedViewController {
    return _selectedViewController = [_viewControllers objectAtIndex:_selectedIndex];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    if (selectedIndex >= _viewControllers.count) return;
    _selectedIndex = selectedIndex;
    
    if (_selectedViewController) {
        [_selectedViewController willMoveToParentViewController:nil];
        [_selectedViewController.view removeFromSuperview];
        [_selectedViewController removeFromParentViewController];
    }
    
    self.selectedViewController = [_viewControllers objectAtIndex:selectedIndex];
    [self addChildViewController:_selectedViewController];
    _selectedViewController.view.frame = [self contentView].bounds;
    [[self contentView] addSubview:_selectedViewController.view];
    [_selectedViewController didMoveToParentViewController:self];
}

- (void) setViewControllers:(NSMutableArray *) viewControllers {
    if (_viewControllers && _viewControllers.count) {
        for (UIViewController *viewController in _viewControllers) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
        }
    }
    
    if (viewControllers && [viewControllers isKindOfClass:[NSArray class]]) {
        _viewControllers = [viewControllers copy];
        [self setSelectedIndex:0];
    } else {
        _viewControllers = nil;
    }
}


#pragma mark -
- (BOOL)prefersStatusBarHidden {
    return true;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void) setStatusBarBackgroundColor:(UIColor *) color {
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) statusBar.backgroundColor = color;
}

#pragma mark -
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return  (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
