//
//  WebRightView.h
//

#import <UIKit/UIKit.h>

typedef void (^MLWebBlackAtIndex) (int index);

@interface MLWebMoreView : UIView

@property (nonatomic, assign) CGFloat fX;
@property (nonatomic, assign) CGSize sContent;
@property (nonatomic, strong) UIColor *wColor;

+ (instancetype) sharedInstance;


/**
 *  @brief 弹起分享信息
 *  @param imgs 标题前的图片
 *  @param titles 显示的标题
 *  @param black   返回选择的id
 */
- (void) showInWindowWithImg:(NSArray *) imgs title:(NSArray *) titles black:(MLWebBlackAtIndex) black;


@end
