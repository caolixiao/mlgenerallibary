//
//  WebRightView.m
//

#import "MLWebMoreView.h"
#import "MLWebRightCell.h"


@interface MLWebMoreView ()<UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray *m_AImg;
    NSMutableArray *m_ATitle;
    MLWebBlackAtIndex _black;
    
    // view
    UIView *vContent;
    UIButton *butClose;
    CGSize sContent;
    CGFloat fEdge;
    CGFloat fH;
    
    UITableView *tableView;
}

@end


@implementation MLWebMoreView

@synthesize fX;
@synthesize sContent;
@synthesize wColor;

+ (instancetype) sharedInstance
{
    static MLWebMoreView *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLWebMoreView alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype) init
{
    if (self = [super init]) {
        fX = CGScale(142);
        fEdge = CGScale(7.0);
        sContent = CGSizeMake(CGScale(179), CGScale(87));
        
        vContent = [[UIView alloc] init];
        vContent.backgroundColor = __RGB(0x1c1b1f);
        [self addSubview:vContent];
        
        
        [self buildHeadView];
    }
    return self;
}


- (void) layoutSubviews {
    [super layoutSubviews];

}

#pragma mark - load view
- (void) buildHeadView {
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorInset = UIEdgeInsetsZero;
    tableView.scrollEnabled = NO;
    tableView.frame = vContent.bounds;
    [vContent addSubview:tableView];
}

#pragma mark - load data


#pragma mark - tableviewDelegate Method 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return m_ATitle.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return sContent.height / m_ATitle.count;
}
- (UITableViewCell *)tableView:(UITableView *) _tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * kIdentifier = @"kIdentifierMLWebRightCell";
    MLWebRightCell * cell = [tableView  dequeueReusableCellWithIdentifier:kIdentifier];
    if (!cell) cell = [[MLWebRightCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifier];
    
    UIImage *img = [UIImage imageNamed:m_AImg[indexPath.row]];
    cell.ivPic.image = img ? img :  [UIImage imgNamed:m_AImg[indexPath.row]];
    cell.labTitle.text = m_ATitle[indexPath.row];
    if (wColor) cell.labTitle.textColor = wColor;
    return cell;
}
- (void)tableView:(UITableView *) _tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _black((int)indexPath.row);
    [self hideView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark -
- (void) showInSuperView:(UIView *) view
{
    self.frame = __SCREEN_BOUNDS;
    if (!view) {
        UIView *presentView = [UIApplication sharedApplication].keyWindow;
        [presentView addSubview:self];
    } else {
        [view addSubview:self];
    }
    
    vContent.frame = CGRectMake(fX, 64.0, sContent.width, 0.0);
    tableView.frame = vContent.bounds;
    self.backgroundColor = __RGBA(0x000000, 0.4);
    [UIView animateWithDuration:0.23 animations:^{
        vContent.frame = CGRectMake(fX, 64.0, sContent.width, sContent.height);
        tableView.frame = vContent.bounds;
    }];
}

- (void) showInWindowWithImg:(NSArray *) imgs title:(NSArray *) titles black:(MLWebBlackAtIndex) black {
    m_AImg = [imgs copy];
    m_ATitle = [titles copy];
    _black = black;
    
    [tableView reloadData];
    [self showInSuperView:[UIView getTopWindow]];
}


-(void)hideView{
    [UIView animateWithDuration:0.23 animations:^{
        vContent.frame = CGRectMake(fX, 64.0, sContent.width, 0.0);
        tableView.frame = vContent.bounds;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.backgroundColor = [UIColor clearColor];
        self.alpha = 1;
        [self removeFromSuperview];
    }];
}

#pragma mark -
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideView];
}

@end
