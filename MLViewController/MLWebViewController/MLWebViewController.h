//
//  MLWebViewController.h
//

#import "MLBaseViewController.h"

typedef void (^MLWebFinishLoad) (NSString *title, NSString *url);

@interface MLWebViewController : MLBaseViewController <UIWebViewDelegate, UINavigationControllerDelegate> {
@private
    UIView *vLeftItem;
    UIButton *butBack;
    UIButton *butClose;
    
@private
    UIWebView *_webView;
    
    BOOL isEnter;
}

@property (nonatomic, strong) NSString *pathURL;
@property (nonatomic, strong) MLWebFinishLoad blockWFL;
@property (nonatomic, assign) BOOL isRegisterClass; 

@end
