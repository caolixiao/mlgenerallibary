//
//  MLWebViewController.m
//

#import "MLWebViewController.h"
#import "MLWebMoreView.h"
#import "MLWebConsole.h"

@implementation MLWebViewController

@synthesize pathURL;

- (void) dealloc {
    INFO_NSLOG(@"dealloc MLWebViewController");
}

- (void) viewDidLoad {
    [super viewDidLoad];
    [self addLeftV:[self buildView]];
    [self setShowTitle:@"浏览器"];
    [self.navigationItem setRightImg:@"MLWebMore"];

    _webView = [[UIWebView alloc] init];
    _webView.delegate = self;
    _webView.scalesPageToFit = true;
    [self.view addSubview:_webView];
    
    isEnter = YES;
}

#pragma makr - 
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isRegisterClass) [NSURLProtocol registerClass:[MLWebConsole class]];
    self.navigationController.delegate = self;
    isEnter = YES;
    
    NSURL *url = [NSURL URLWithString:pathURL];
    if (!url) url = [NSURL fileURLWithPath:pathURL];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [_webView loadRequest:request];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (_isRegisterClass) [NSURLProtocol unregisterClass:[MLWebConsole class]];
    isEnter = NO;
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.delegate = nil;
}


- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _webView.frame = self.view.bounds;
}


#pragma mark - view
 - (UIView *) buildView {
     vLeftItem = [[UIView alloc] init];
//     vLeftItem.frame = CGRectMake(0.0, 0.0, 80.0, 44.0);
//     vLeftItem.frame = CGRectMake(0.0, 0.0, 40.0, 44.0);
    
     butBack = [UIButton buttonWithType:UIButtonTypeCustom];
     butBack.titleLabel.font = [UIFont systemFontOfSize:12];
     [butBack setTitle:@"返回" forState:UIControlStateNormal];
     [butBack setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateNormal];
     
     UIImage *img = [[UIImage imageNamed:@"nav_back_normal"] imageWithAlpha:0.8];
     [butBack setImage:img forState:UIControlStateNormal];
     [butBack setImage:[[UIImage imageNamed:@"nav_back_selected"] imageWithAlpha:0.8] forState:UIControlStateHighlighted];
     
     butBack.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 40.0 - (img.size.width));
     butBack.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
     [butBack addTarget:self action:@selector(actionBack:) forControlEvents:UIControlEventTouchUpInside];
     [vLeftItem addSubview:butBack];
     
     
     
     butClose = [UIButton buttonWithType:UIButtonTypeCustom];
     butClose.titleLabel.font = [UIFont systemFontOfSize:12];
     [butClose setTitle:@"关闭" forState:UIControlStateNormal];
     [butClose setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateNormal];
//     [butClose setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4] forState:UIControlStateNormal];
     [butClose addTarget:self action:@selector(actionClose:) forControlEvents:UIControlEventTouchUpInside];
     [vLeftItem addSubview:butClose];

//     butBack.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(vLeftItem.frame), CGRectGetHeight(vLeftItem.frame));
//     butBack.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(vLeftItem.frame)/2, CGRectGetHeight(vLeftItem.frame));
//     butClose.frame = CGRectMake(CGRectGetWidth(vLeftItem.frame)/2.0, 0.0, CGRectGetWidth(vLeftItem.frame)/2.0, CGRectGetHeight(vLeftItem.frame));
     
     [self isHiddenClose:true];
    
     return vLeftItem;
}

- (void) isHiddenClose:(BOOL) isHidden {
    butClose.hidden = isHidden;
    CGFloat fW = 80;
    CGFloat fH = 44;
    
    if (isHidden) {
        vLeftItem.frame = CGRectMake(VIEWX(vLeftItem), 0, fW/2.0, fH);
        butBack.frame = CGRectMake(0, 0, fW/2.0, fH);
    }else {
        vLeftItem.frame = CGRectMake(VIEWX(vLeftItem), 0, fW, fH);
        butBack.frame = CGRectMake(0, 0, fW/2.0, fH);
        butClose.frame = CGRectMake(fW/2.0, 0, fW/2.0, fH);
    }
}


#pragma mark - action Event
- (void) actionBack:(UIButton *) button {
    if (_webView.canGoBack) [_webView goBack];
    else [self.navigationController popViewControllerAnimated:YES];
}

- (void) actionClose:(UIButton *) button {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) actionMore:(UIButton *) button {
    NSArray *m_AImg = @[@"MLWeb_02", @"MLWeb_03"];
    NSArray *m_ATitle = @[@"在浏览器中打开", @"复制链接"];
    
    __weak MLWebViewController *weakSelf = self;
    [[MLWebMoreView sharedInstance] showInWindowWithImg:m_AImg title:m_ATitle black:^(int index) {
        MLWebViewController *blackSelf = weakSelf;
        NSString *sURL = blackSelf->_webView.request.URL.absoluteString;
        sURL = sURL ? sURL : blackSelf.pathURL;
        
        if (index == 0) {
            NSURL *url = [NSURL URLWithString:sURL];
            if ([[UIApplication sharedApplication] canOpenURL:url])
                [[UIApplication sharedApplication] openURL:url];
//                [[UIApplication sharedApplication] openURL:url options:nil completionHandler:NULL];
        }
        else if (index == 1) {
            [UIPasteboard generalPasteboard].string = sURL;
        }
    }];
}

#pragma mark - super action
- (void) rightItemButton:(UIButton *) button {
    [self actionMore:button];
}

#pragma mark -
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self setShowTitle:title];
    [self isHiddenClose:!_webView.canGoBack];
    
//    NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];
    if (_blockWFL) _blockWFL(title, webView.request.URL.absoluteString);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (isEnter) {
        [navigationController.navigationBar setWebBackground];
    }else {
        [navigationController.navigationBar setDefaultBackground];
    }
}



#pragma mark -
- (UIStatusBarAnimation) preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)prefersStatusBarHidden {
    return false;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
