//
//  MLSplitViewController.h
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(int, MLSplitDirection) {
    MLSplitDirectionLeft  = 0,
    MLSplitDirectionRight = 1,
    MLSplitDirectionUp    = 2,
    MLSplitDirectionDown  = 3,
};

#pragma mark - left item
@interface MLSplitLeftItem : UIControl {
    UIImageView *ivBKG;
    UIImageView *ivIcon;
    UILabel *labtitle;
    UIImageView *ivLine;
}

@property (nonatomic, strong) UIImage *backgroundImg;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) UIImage *selectedBackgroundImg;
@property (nonatomic, strong) NSString *selectedTitle;
@property (nonatomic, strong) UIImage *selectedImage;

@end

#pragma mark - left view
// 左边的view
@class MLSplitLeftView;
@protocol MLSplitLeftViewDelegate <NSObject>

- (BOOL) splitLeftView:(MLSplitLeftView *) splitLeftView shouldSelectItemAtIndex:(NSInteger)index;
- (void) splitLeftView:(MLSplitLeftView *) splitLeftView didSelectItemAtIndex:(NSInteger)index;

@end

@interface MLSplitLeftView : UIView

@property (nonatomic, weak) id <MLSplitLeftViewDelegate> delegate;

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, getter=isTranslucent) BOOL translucent;

@property (nonatomic, strong) UIImageView *ivRightLine;

@property (nonatomic, copy) NSArray *items;
@property (nonatomic, weak) MLSplitLeftItem *selectedItem;
@property (nonatomic) NSUInteger selectedIndex;

@end


#pragma mark -
@class MLSplitViewController;
@protocol MLSplitViewControllerDataSource;

// 显示和行为
@protocol MLSplitViewControllerDelegate <NSObject>

@optional
- (CGFloat) widthForLeftAndRightAtLeft;

- (BOOL) splitViewController:(MLSplitViewController *) splitViewController shouldSelectViewController:(UIViewController *)viewController;

- (void) splitViewController:(MLSplitViewController *) splitViewController didSelectViewController:(UIViewController *)viewController;

@end


#pragma mark - MLSplitViewController
@interface MLSplitViewController : UIViewController

@property (nonatomic, weak) id<MLSplitViewControllerDelegate> delegate;

@property (nonatomic) MLSplitDirection direction;

@property (nonatomic, strong) NSArray *viewControllers;

@property (nonatomic, strong) UIViewController *selectedViewController;

@property (nonatomic) NSUInteger selectedIndex;

/**
 * @brief 重载可以改变左边的宽度
 */
- (CGFloat) widthForLeftAndRightAtLeft;

/**
 * @brief 重载自定义左边的view，默认为现在的样式
 *        他会掉用多次，包括改大小的时候
 *        默认的view是  MLSplitLeftView
 */
- (UIView *) buildLeftView;

- (void) addItems:(NSArray *) items;

@end

