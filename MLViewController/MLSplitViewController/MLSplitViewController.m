//
//  MLSplitViewController.m
//

#import "MLSplitViewController.h"


@implementation MLSplitLeftItem : UIControl

@synthesize backgroundImg = _backgroundImg;
@synthesize title = _title;
@synthesize image= _image;

- (instancetype) init {
    if (self = [super init]) {
        self.clipsToBounds = YES;
        [self buildView];
        self.selected = NO;
    }
    return self;
}

- (void) setSelected:(BOOL)selected {
    [super setSelected:selected];
 
    if (selected)
    {
        if (_selectedBackgroundImg) ivBKG.image = _selectedBackgroundImg;
        if(_selectedImage) ivIcon.image = _selectedImage;
        if(_selectedTitle) labtitle.text = _selectedTitle;
    }
    else
    {
        if (_backgroundImg) ivBKG.image = _backgroundImg;
        if(_image) ivIcon.image = _image;
        if(_title) labtitle.text = _title;
    }
}

- (void) layoutSubviews {
    if (ivBKG) ivBKG.frame = self.bounds;
    if (ivIcon) ivIcon.frame = CGRectMake(0.0f, 0.0f, VIEWH(self), VIEWH(self));
    if (labtitle) labtitle.frame = CGRectMake(VIEWH(self)/2.0f, 0.0f, VIEWW(self) - VIEWH(self)/2.0f, VIEWH(self));
    if (ivLine) ivLine.frame = CGRectMake(0.0f, VIEWH(self) - 1.0f, VIEWW(self), 1.0f);
}

- (void) buildView {
    ivBKG = [[UIImageView alloc] init];
    ivBKG.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:ivBKG];
    
    ivIcon = [[UIImageView alloc] init];
    ivIcon.contentMode = UIViewContentModeCenter;
    [self addSubview:ivIcon];
    
    labtitle = [[UILabel alloc] init];
    labtitle.numberOfLines = 1;
    labtitle.textAlignment = NSTextAlignmentCenter;
    labtitle.font = [UIFont systemFontOfSize:20.0f];
    labtitle.textColor = [UIColor whiteColor];
    [self addSubview:labtitle];
    
    ivLine = [[UIImageView alloc] init];
//    ivLine.image = [PubGlobal image:[UIImage imageNamed:@"Line"] rotation:UIImageOrientationLeft];
    [self addSubview:ivLine];
    
    if (self.selected)
    {
        if (_selectedBackgroundImg) ivBKG.image = _selectedBackgroundImg;
        if(_selectedImage) ivIcon.image = _selectedImage;
        if(_selectedTitle) labtitle.text = _selectedTitle;
    }
    else
    {
        if (_backgroundImg) ivBKG.image = _backgroundImg;
        if(_image) ivIcon.image = _image;
        if(_title) labtitle.text = _title;
    }

}

- (void) setBackgroundImg:(UIImage *)backgroundImg {
    _backgroundImg = backgroundImg;
    if (!self.selected && ivBKG) ivBKG.image = backgroundImg;
}

- (void) setTitle:(NSString *) title {
    _title = title;
    if(!self.selected && labtitle) labtitle.text = title;
}

- (void) setImage:(UIImage *)image {
    _image = image;
    if (!self.selected && ivIcon) ivIcon.image = image;
}

- (void) setSelectedBackgroundImg:(UIImage *)selectedBackgroundImg {
    _selectedBackgroundImg = selectedBackgroundImg;
    if (self.selected && ivBKG) ivBKG.image = selectedBackgroundImg;
}

- (void) setSelectedTitle:(NSString *)selectedTitle {
    _selectedTitle = selectedTitle;
    if (self.selected && labtitle) labtitle.text = selectedTitle;
}

- (void) setSelectedImage:(UIImage *)selectedImage{
    _selectedImage = selectedImage;
    if (self.selected && ivIcon) ivIcon.image = selectedImage;
}

@end


@implementation MLSplitLeftView

@synthesize delegate;

@synthesize backgroundView = _backgroundView;
@synthesize translucent = _translucent;

@synthesize ivRightLine = _ivRightLine;

@synthesize items = _items;
@synthesize selectedItem = _selectedItem;
@synthesize selectedIndex = _selectedIndex;

- (instancetype) init {
    if (self = [super init]) {
        _backgroundView = [[UIView alloc] init];
        [self addSubview:_backgroundView];
        
        _ivRightLine = [[UIImageView alloc] init];
        _ivRightLine.image = [UIImage imageNamed:@"Line"];
        [self addSubview:_ivRightLine];
    }
    return self;
}

- (void) layoutSubviews {
    _backgroundView.frame = self.bounds;
    _ivRightLine.frame = CGRectMake(VIEWW(self) - 1.0f, 0.0f, 1.0f, VIEWH(self));

    int count = _items ? (int)_items.count : 0;
    CGFloat fH = roundf(VIEWH(self)/count);
    int index = 0;
    for (UIView *item in _items) {
        item.frame = CGRectMake(0.0f, fH * index, VIEWW(self), fH);
        index++;
    }
}

#pragma mark - Translucency
- (void)setTranslucent:(BOOL)translucent {
    _translucent = translucent;
    [_backgroundView setBackgroundColor:__RGBA(0xffffff, (translucent ? 0.8 : 1.0))];
}

#pragma mark - Configuration
- (void)setItems:(NSArray *)items {
    for (MLSplitLeftItem *item in _items) [item removeFromSuperview];
    
    _items = [items copy];
    for (MLSplitLeftItem *item in _items) {
        [item addTarget:self action:@selector(actionClickWithItem:) forControlEvents:UIControlEventTouchDown];
        [self addSubview:item];
    }
}

- (void) setSelectedItem:(MLSplitLeftItem *)selectedItem {
    if (selectedItem == _selectedItem) return;
    
    _selectedItem.selected = NO;
    
    _selectedItem = selectedItem;
    _selectedItem.selected = YES;
}

- (void) setSelectedIndex:(NSUInteger)selectedIndex {
    if (!_items && selectedIndex >= _items.count) return;
    
    _selectedIndex = selectedIndex;
    [self actionClickWithItem:[_items objectAtIndex:_selectedIndex]];
}


#pragma mark - Item selection
- (void) actionClickWithItem:(id) sender {
    _selectedIndex = [_items indexOfObject:sender];
    if ([delegate respondsToSelector:@selector(splitLeftView:shouldSelectItemAtIndex:)]) {
        if (![delegate splitLeftView:self shouldSelectItemAtIndex:_selectedIndex]) return;
    }
    
    if ([delegate respondsToSelector:@selector(splitLeftView:didSelectItemAtIndex:)]) {
        [delegate splitLeftView:self didSelectItemAtIndex:_selectedIndex];
    }
    
    self.selectedItem = sender;
}

@end


#pragma mark - -
@interface MLSplitViewController () <MLSplitLeftViewDelegate>
{
    UIView *_leftView;
    UIView *_contentView;
    
    CGFloat fW_Left;
    
    // content
    UIView *_curLeftView;
    MLSplitLeftView *_vLeft;
}

@end

@implementation MLSplitViewController

@synthesize delegate;

@synthesize direction = _direction;

@synthesize viewControllers = _viewControllers;
@synthesize selectedViewController = _selectedViewController;
@synthesize selectedIndex = _selectedIndex;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // ============================================================
    // init data
    [self loadInitData];
    
    // ============================================================
    // init view
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:[self leftView]];
    [self.view addSubview:[self contentView]];
    
    _curLeftView = [self buildLeftView];
    [[self leftView] addSubview:_curLeftView];
    
    [self buildRightView];
    
    // ============================================================
    //
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_vLeft) _vLeft.selectedIndex = _selectedIndex;
    else self.selectedIndex = _selectedIndex;
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self showFrameChange];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    _leftView = nil;
    _contentView = nil;
    _curLeftView = nil;
    _vLeft = nil;
}

#pragma mark - 
- (CGFloat) widthForLeftAndRightAtLeft {
    return VIEWW(self.view)/4.0;
}

#pragma mark -
- (void) loadInitData {
    _selectedIndex = 0;
    
    [self showFrameChange];
}

- (void) showFrameChange {
    if (_direction == MLSplitDirectionLeft) {
        if ([delegate respondsToSelector:@selector(widthForLeftAndRightAtLeft)])
            fW_Left = [delegate widthForLeftAndRightAtLeft];
        else
            fW_Left = [self widthForLeftAndRightAtLeft];
        
        [self leftView].frame = CGRectMake(0.0f, 0.0f, fW_Left, VIEWH(self.view));
        [self contentView].frame = CGRectMake(fW_Left, 0.0f, VIEWW(self.view) - fW_Left, VIEWH(self.view));
        
        // content
        if (_curLeftView) _curLeftView.frame = [self leftView].bounds;
    }
    else {
        
    }
}

#pragma mark - 
- (UIView *) buildLeftView {
    if (!_vLeft) {
        _vLeft = [[MLSplitLeftView alloc] init];
        _vLeft.backgroundView.backgroundColor = [UIColor clearColor];
        _vLeft.delegate = self;
    }    
    return _vLeft;
}

- (void) addItems:(NSArray *) items {
    if (items.count > 0 && _vLeft) _vLeft.items = items;
}

- (void) buildRightView {

}

#pragma mark - view
- (UIView *) leftView {
    if (!_leftView) {
        _leftView = [[UIView alloc] init];
        [_leftView setBackgroundColor:[UIColor clearColor]];
    }
    return _leftView;
}

- (UIView *) contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        [_contentView setBackgroundColor:[UIColor clearColor]];
    }
    return _contentView;
}

#pragma mark - view controller
- (UIViewController *)selectedViewController {
    return _selectedViewController = [_viewControllers objectAtIndex:_selectedIndex];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    if (selectedIndex >= self.viewControllers.count) return;
    _selectedIndex = selectedIndex;
    
    if (_selectedViewController) {
        [_selectedViewController willMoveToParentViewController:nil];
        [_selectedViewController.view removeFromSuperview];
        [_selectedViewController removeFromParentViewController];
    }
    
    self.selectedViewController = [_viewControllers objectAtIndex:selectedIndex];
    [self addChildViewController:_selectedViewController];
    _selectedViewController.view.frame = [self contentView].bounds;
    [[self contentView] addSubview:_selectedViewController.view];
    [_selectedViewController didMoveToParentViewController:self];
}

- (void) setViewControllers:(NSMutableArray *) viewControllers {
    if (_viewControllers && _viewControllers.count) {
        for (UIViewController *viewController in _viewControllers) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
        }
    }
    
    if (viewControllers && [viewControllers isKindOfClass:[NSArray class]]) {
        _viewControllers = viewControllers;
    } else {
        _viewControllers = nil;
    }
}

- (void) setDirection:(MLSplitDirection)direction {
    [self showFrameChange];
}

#pragma mark - MLSplitLeftViewDelegate
- (BOOL) splitLeftView:(MLSplitLeftView *) splitLeftView shouldSelectItemAtIndex:(NSInteger)index {
    if ([delegate respondsToSelector:@selector(splitLeftView:shouldSelectItemAtIndex:)]) {
        if (![delegate splitViewController:self shouldSelectViewController:_selectedViewController]) return NO;
    }
    
    if (_selectedViewController == _viewControllers[index]) {
        if ([_selectedViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navControl = (UINavigationController *)_selectedViewController;
            if ([navControl topViewController] != navControl.viewControllers[0])
                [navControl popToRootViewControllerAnimated:YES];
        }
        return NO;
    }
    
    return YES;

}

- (void) splitLeftView:(MLSplitLeftView *) splitLeftView didSelectItemAtIndex:(NSInteger)index {
    if (index < 0 || index >= _viewControllers.count) return;

    [self setSelectedIndex:index];
    
    if ([delegate respondsToSelector:@selector(splitViewController:didSelectViewController:)]) {
        [delegate splitViewController:self didSelectViewController:[_viewControllers objectAtIndex:index]];
    }
}


#pragma mark -
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

@end
