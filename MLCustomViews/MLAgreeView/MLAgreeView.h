//
//  MLAgreeView.h
//

#import <UIKit/UIKit.h>

typedef void (^MLBlockAgreeView) (BOOL isAgree, int index);

@interface MLAgreeView : UIView

+ (instancetype) sharedInstance;

- (void) setPathURL:(NSString *) pathURL;

- (void) showInWindowWithblack:(MLBlockAgreeView) black;

- (void) hideView;

@end
