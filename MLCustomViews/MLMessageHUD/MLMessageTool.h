//
//  MLMessageTool.h

#import <Foundation/Foundation.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface MLMessageTool : NSObject<MBProgressHUDDelegate>{
    MBProgressHUD *showMessage;
}
@property (nonatomic,strong)  MBProgressHUD *showMessage;

+ (void)showMessage:(NSString *)message isError:(BOOL)yesOrNo;
+ (void)showMessage:(NSString *)message view:(UIView*)view isError:(BOOL)yesOrNo;
+ (MBProgressHUD *)showProcessMessage:(NSString *)message;
+ (MBProgressHUD *)showProcessMessage:(NSString *)message view:(UIView*)view;

+ (void) showFailMessage:(NSString *) message defaultShow:(NSString *) content;
+ (void) showPromptMessage:(NSString *) message;


+(void)hide;

+(void)hideAfterDelay:(NSTimeInterval)delay;
 

@end
