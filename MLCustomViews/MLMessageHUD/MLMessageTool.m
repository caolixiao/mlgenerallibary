//
//  MLMessageTool.m

#import "MLMessageTool.h"
#import "UIView+Toast.h"

@interface MLMessageTool()
+(MLMessageTool *)shareInstance;
-(void)buildProcessMessage:(NSString *)message;
-(void)buildProcessMessage:(NSString *)message  view:(UIView*)view;
-(void)buildMessage:(NSString *)message isError:(BOOL)yesOrNo;
-(void)buildMessage:(NSString *)message view:(UIView*)view  isError:(BOOL)yesOrNo;
@end

@implementation MLMessageTool

@synthesize showMessage;

+(MLMessageTool *)shareInstance{
    static MLMessageTool *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLMessageTool alloc] init];
    });
    
    return sharedInstance;
    
}

-(void)showDuration{
	[showMessage hideAnimated:YES afterDelay:1.68f];
    
}
-(void)buildMessage:(NSString *)message view:(UIView*)view  isError:(BOOL)yesOrNo{
    [showMessage removeFromSuperview];
    showMessage=[[MBProgressHUD alloc] initWithView:view];
    if(!yesOrNo)
        [showMessage setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success.png"]]];
    else
        [showMessage setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error.png"]]];
    [showMessage setMode:MBProgressHUDModeCustomView];
    [view addSubview:showMessage];
    showMessage.label.text=message;
    [showMessage showAnimated:YES];
    [self performSelectorOnMainThread:@selector(showDuration) withObject:nil waitUntilDone:YES];
}
-(void)buildMessage:(NSString *)message isError:(BOOL)yesOrNo{
    [showMessage removeFromSuperview];
    showMessage=[[MBProgressHUD alloc] initWithView:[self getTopLevelWindow]];
    if(!yesOrNo)
        [showMessage setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success.png"]]];
    else
        [showMessage setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error.png"]]];
    [showMessage setMode:MBProgressHUDModeCustomView];
    
    [[self getTopLevelWindow] addSubview:showMessage];
    showMessage.label.text=message;
    [showMessage showAnimated:YES];
    [self performSelectorOnMainThread:@selector(showDuration) withObject:nil waitUntilDone:YES];
}

+(void)hide{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance hide];
}

+(void)hideAfterDelay:(NSTimeInterval)delay
{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance hideAfterDelay:delay];
}
-(void)hide{
    [showMessage hideAnimated:YES];
}

-(void)hideAfterDelay:(NSTimeInterval)delay
{
    [showMessage hideAnimated:YES afterDelay:delay];
}

-(UIWindow *)getTopLevelWindow{
    UIWindow *window=nil;
    for (UIWindow *_window in [[UIApplication sharedApplication] windows]) {
        if(window==nil){
            window=_window;
        }
        if(_window.windowLevel>window.windowLevel){
            window=_window;
        }
    }
    return window;
}

-(void)buildProcessMessage:(NSString *)message{
    [showMessage removeFromSuperview];
    showMessage=[[MBProgressHUD alloc] initWithView:[self getTopLevelWindow]];
    showMessage.animationType = MBProgressHUDAnimationZoom;
    [[self getTopLevelWindow] addSubview:showMessage];
    showMessage.label.text=message;
    [showMessage showAnimated:YES];
}
-(void)buildProcessMessage:(NSString *)message  view:(UIView*)view{
    [showMessage removeFromSuperview];
    showMessage=[[MBProgressHUD alloc] initWithView:view];
    showMessage.animationType = MBProgressHUDAnimationZoom;
    [view addSubview:showMessage];
    showMessage.label.text=message;
    [showMessage showAnimated:YES];
}
 
+ (void)showMessage:(NSString *)message view:(UIView*)view isError:(BOOL)yesOrNo{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildMessage:message view:view isError:yesOrNo];
}
+ (void)showMessage:(NSString *)message isError:(BOOL)yesOrNo{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildMessage:message isError:yesOrNo];
}
+ (MBProgressHUD *)showProcessMessage:(NSString *)message{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildProcessMessage:message];
    return sharedInstance.showMessage;
}
+ (MBProgressHUD *)showProcessMessage:(NSString *)message view:(UIView*)view{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildProcessMessage:message view:view];
    return sharedInstance.showMessage;
}


#pragma mark - 

-(void)buildNetFailMessage:(NSString *)message defaultShow:(NSString *) content
{
    if (message)
        [[self getTopLevelWindow] makeToast:message duration:2 position:CSToastPositionBottom];
    else if (content)
        [[self getTopLevelWindow] makeToast:content duration:2 position:CSToastPositionBottom];
}

+ (void) showFailMessage:(NSString *) message defaultShow:(NSString *) content
{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildNetFailMessage:message defaultShow:content];
}

+ (void) showPromptMessage:(NSString *) message
{
    MLMessageTool *sharedInstance =[MLMessageTool shareInstance];
    [sharedInstance buildNetFailMessage:message defaultShow:nil];
}

@end
