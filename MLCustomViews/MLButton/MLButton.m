//
//  MLButton.m
//

#import "MLButton.h"

@implementation MLButton

@synthesize y;
@synthesize iw;

- (CGRect) imageRectForContentRect:(CGRect)contentRect
{
    CGRect rect = contentRect;
    rect.origin.x = (iw == 0 ? 0.0f : (CGRectGetWidth(self.bounds) - iw)/2.0f);
    rect.origin.y = y - iw/2.0 + CGScale(10.0);
    rect.size.width = (iw == 0 ? CGRectGetWidth(self.bounds) : iw);
    rect.size.height = (iw == 0 ? CGRectGetWidth(self.bounds) : iw);
    
    return rect;
}

- (CGRect) titleRectForContentRect:(CGRect)contentRect
{
    CGRect rect = contentRect;
    rect.origin.x = - (VIEWW(self)/2.0f);
    rect.origin.y = y - iw/2.0 + CGScale(10.0) + iw + (iw - CGScale(20.0))/2.0;
    rect.size.width = VIEWW(self)*2;
    rect.size.height = CGScale(20.0);
    return rect;
}

@end
