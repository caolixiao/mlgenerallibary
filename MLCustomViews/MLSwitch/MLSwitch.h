//
//  MLSwitch.h
//

#import <UIKit/UIKit.h>

@interface MLSwitch : UIView 

@property (nonatomic, strong, nullable) UISwitch *sw;

- (void)addTarget:(nullable id)target action:(nullable SEL)action forControlEvents:(UIControlEvents)controlEvents;

@end
