//
//  MLSwitch.m
//

#import "MLSwitch.h"


@implementation MLSwitch

@synthesize sw;

- (instancetype) init {
    if (self = [super init]) {
        self.layer.masksToBounds = true;
        
        sw = [[UISwitch alloc] init];
        sw.tintColor = self.backgroundColor;
        [self addSubview:sw];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = VIEWH(self)/2.0;
    
    CGFloat fW = VIEWH(self)/31.0;
    sw.transform = CGAffineTransformMakeScale(fW, fW);
    
    sw.frame = CGRectZero;
}

- (void)addTarget:(nullable id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents {
    [sw addTarget:target action:action forControlEvents:controlEvents];
}


@end
