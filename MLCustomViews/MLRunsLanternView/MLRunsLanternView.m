//
//  MLRunsLanternView.m
//

#import "MLRunsLanternView.h"

@interface MLRunsLanternView () {
    NSMutableString *cStr;
    
    UIView *_superview;
    
    MLRunsLanternViewAtClick _click;
}

@end

@implementation MLRunsLanternView



+ (instancetype) sharedInstance
{
    static MLRunsLanternView *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLRunsLanternView alloc] init];
    });
    
    return sharedInstance;
}

+ (void) superview:(UIView *) view {
    [MLRunsLanternView sharedInstance]->_superview = view;
}

+ (void) showRLView:(UIView *) view frame:(CGRect) frame content:(NSString *) content {
    [[MLRunsLanternView sharedInstance] showRLView:view frame:frame content:content];
}

+ (void) showRLView:(UIView *) view content:(NSString *) content {
    [MLRunsLanternView showRLView:view frame:CGRectMake(0, (64), CGScale(375), CGScale(18)) content:content];
}

+ (void) showRL:(NSString *) content {
    [MLRunsLanternView showRLView:nil frame:CGRectMake(0, (64), CGScale(375), CGScale(18)) content:content];
}

+ (void) setContent:(NSString *) content {
    if (content && content.length > 0) [[MLRunsLanternView sharedInstance] setContent:content];
}

+ (void) cancelShow {
    [[MLRunsLanternView sharedInstance] hideView];
}

+ (void) setHidden:(BOOL) isHidden {
    [MLRunsLanternView sharedInstance].hidden = isHidden;
}

+ (void) didClick:(MLRunsLanternViewAtClick) click {
    [MLRunsLanternView sharedInstance]->_click = click;
}

#pragma mark -
- (instancetype) init {
    if (self = [super init]) {
        self.backgroundColor = __RGB(0xFFFFBF);
        cStr = [NSMutableString stringWithCapacity:0];

    }
    return self;
}

- (UILabel *) buildBaseView {
    UILabel *labContent = [[UILabel alloc] init];
    labContent.frame = CGRectZero;
    labContent.font = [UIFont systemFontOfBaseSize:10];
    labContent.textColor = __RGB(0xD94B25);
    labContent.numberOfLines = 1;
    labContent.textAlignment = NSTextAlignmentCenter;
    labContent.userInteractionEnabled = NO;
    
    return labContent;
}

- (void) hideView {
    self.alpha = 1;
    [UIView animateWithDuration:0.23 animations:^{
        self.frame = (CGRect){self.frame.origin, {self.frame.size.width, 0}};
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) showRLView:(UIView *) view frame:(CGRect) frame content:(NSString *) content {
    if (!view) {
        UIView *presentView = _superview ? _superview : [UIApplication sharedApplication].keyWindow;
        if (_superview) frame = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
        [presentView addSubview:self];
    } else {
        [view addSubview:self];
    }

    self.frame = (CGRect){frame.origin, {frame.size.width, 0}};
    self.alpha = 0;
    [UIView animateWithDuration:0.23 animations:^{
        self.frame = frame;
        self.alpha = 1;
    } completion:^(BOOL finished) {
        [self runsLantern:content];
    }];
}

- (void) setContent:(NSString *) content {
    if (self.superview) {
        [cStr appendString:content];
    }else {
        [MLRunsLanternView showRL:content];
    }
}

- (void) runsLantern:(NSString *) content {
    UILabel *label = [self buildBaseView];
    label.text = content;
    [self insertSubview:label atIndex:0];
    
    [label sizeToFit];
    CGFloat fW = VIEWW(label);
    label.frame = CGRectMake(VIEWW(self), 0, fW, VIEWH(self));
    
    double duration = fW < CGScale(375) ? 10 : 10 * (fW/CGScale(375));
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        label.frame = CGRectMake(-fW, 0, fW, VIEWH(label));
    }completion:^(BOOL finished) {
// 动画的点 label.layer.presentationLayer.position
        if (cStr && cStr.length > 0) {
            [self runsLantern:cStr];
            [cStr setString:@""];
            [label removeFromSuperview];
        }else {
            [self hideView];
        }
    }];
}


#pragma mark -
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [self hideView];
    if (_click) _click();
}

@end
