//
//  MLNavViewBar.m
//

#import "MLNavViewBar.h"

#define __SCREEN_BOUNDS             [[UIScreen mainScreen] bounds]

const NSUInteger Offset_TAG = 10000;
const NSUInteger Offset_Line_TAG = 20000;

NSString *const kMLSelectStateNormal_Img        = @"kMLSelectStateNormal_Img";
NSString *const kMLSelectStateSelected_Img      = @"kMLSelectStateSelected_Img";
NSString *const kMLSelectStateHighlighted_Img   = @"kMLSelectStateHighlighted_Img";

NSString *const kMLSelectStateNormal_Color      = @"kMLSelectStateNormal_Color";
NSString *const kMLSelectStateSelected_Color    = @"kMLSelectStateSelected_Color";
NSString *const kMLSelectStateHighlighted_Color = @"kMLSelectStateHighlighted_Color";

NSString *const kMLSelectStateNormal_Content_Color      = @"kMLSelectStateNormal_Content_Color";
NSString *const kMLSelectStateSelected_Content_Color    = @"kMLSelectStateSelected_Content_Color";
NSString *const kMLSelectStateHighlighted_Content_Color = @"kMLSelectStateHighlighted_Content_Color";

NSString *const kMLSelectStateNormal            = @"kMLSelectStateNormal";
NSString *const kMLSelectStateSelected          = @"kMLSelectStateSelected";
NSString *const kMLSelectStateHighlighted       = @"kMLSelectStateHighlighted";

@interface MLNavViewBar ()
{
    UIImageView *ivBKG;
    UIView *vButton;
    UIView *vLineTop;
    UIView *vLineBottom;
    
    // 保存的 （kMLSelectStateNormal:array, t_Selected:array, t_Highlighted:array, img_Normal:array, img_Selected:array, img_Highlighted:array, ）
    NSMutableDictionary *m_Data;
    
    UIButton *current_Button;
}

@end


@implementation MLNavViewBar

@synthesize toal = _toal;
@synthesize delegate = _delegate;
@synthesize titleFont = _titleFont;
@synthesize isShowTopLine = _isShowTopLine;
@synthesize isShowBottomLine = _isShowBottomLine;

@synthesize isShowSplitLine = _isShowSplitLine;

@synthesize selectedIndex = _selectedIndex;

@synthesize fH_Line = _fH_Line;
@synthesize edge_width = _edge_width;
@synthesize fW_Edge;

- (instancetype) initWithCoder:(NSCoder *) aDecoder
{
    if(self = [super initWithCoder:aDecoder]) {
        // loading code
        [self initDefaultData];
        
        //set background iamge
        self.frame = CGRectMake(0.0f, 0.0f, __SCREEN_BOUNDS.size.width, _height);
        [self initDefaultView];
    }
    return self;
}

- (instancetype) init
{
    if (self = [super init]) {
        // loading code
        [self initDefaultData];
        
        //set background iamge
        self.frame = CGRectMake(0.0f, 0.0f, __SCREEN_BOUNDS.size.width, _height);
        [self initDefaultView];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // loading code
        [self initDefaultData];
        
        //set background iamge
        self.frame = CGRectMake(0.0f, 0.0f, frame.size.width, _height);
        [self initDefaultView];
        
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    _height = self.frame.size.height;
    if (vButton)  {
        vButton.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height);
        // load button
        float but_width = (VIEWW(self) - _edge_width * 2)/_toal - fW_Edge/2.0f;
        
        int i = 0;
        if (_fH_Line < 0) _fH_Line = _height;
        float fY = (_height - _fH_Line)/2.0;
        for (UIButton *but in vButton.subviews) {
            but.frame = CGRectMake(_edge_width + ((but_width + fW_Edge) * i), 1.0f, but_width, _height - 1.0f);
            
            UIView *vTmpLine = [self viewWithTag:Offset_Line_TAG + i];
            if ([vTmpLine isKindOfClass:[UIView class]])
                vTmpLine.frame = CGRectMake(VIEWMaxX(but) - 0.5, fY, 1.0, _fH_Line);
            
            i++;
        }
    }
    if (ivBKG) ivBKG.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height);
    if (vLineTop) vLineTop.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, 1.0f);
    if (vLineBottom) vLineBottom.frame = CGRectMake(0.0f, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
}

- (void) initDefaultView
{
    if (!vButton)
    {
        vButton = [[UIView alloc] init];
        [self addSubview:vButton];
    }
    vButton.frame = self.bounds;
}

- (void) setIsShowTopLine:(BOOL)isShowTopLine
{
    _isShowTopLine = isShowTopLine;
    
    if (_isShowTopLine) {
        if (!vLineTop) {
            vLineTop = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, VIEWW(self), 1.0f)];
            vLineTop.backgroundColor = _colorLine ? _colorLine : __RGB(0xdedede);
            [self insertSubview:vLineTop atIndex:0];
        }
    }
    else {
        if (vLineTop) {
            [vLineTop removeFromSuperview];
            vLineTop = nil;
        }
    }
}

- (void) setIsShowBottomLine:(BOOL)isShowBottomLine
{
    _isShowBottomLine = isShowBottomLine;
    
    if (_isShowBottomLine) {
        if (!vLineBottom) {
            vLineBottom = [[UIView alloc] initWithFrame:CGRectMake(0.0f, VIEWH(self) - 1.0f, VIEWW(self), 1.0f)];
            vLineBottom.backgroundColor = _colorLine ? _colorLine : __RGB(0xdedede);
            [self insertSubview:vLineBottom atIndex:0];
        }
    }
    else {
        if (vLineBottom) {
            [vLineBottom removeFromSuperview];
            vLineBottom = nil;
        }
    }
}

- (void) setIsShowSplitLine:(BOOL)isShowSplitLine {
    _isShowSplitLine = isShowSplitLine;
    
    if (_isShowSplitLine) {
        for (int i = 0; i < _toal - 1; i++) {
            UIView *vTmpLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, VIEWH(self) - 1.0f, VIEWW(self), 1.0f)];
            vTmpLine.tag = (i+Offset_Line_TAG);
            vTmpLine.backgroundColor = _colorSplitLine ? _colorSplitLine : __RGB(0xdedede);
            [self insertSubview:vTmpLine atIndex:0];
        }
    }
    else {
        for (UIView *vTmpLine in self.subviews) {
            if (vTmpLine.tag >= Offset_Line_TAG && vTmpLine.tag <Offset_Line_TAG+_toal) {
                [vTmpLine removeFromSuperview];
            }
        }
    }
}

- (void) setColorLine:(UIColor *)colorLine {
    _colorLine = colorLine;
    if (vLineBottom) vLineBottom.backgroundColor = colorLine;
    if (vLineTop) vLineTop.backgroundColor = colorLine;
}

- (void) initDefaultData
{
    _edge_width = CGScale(10.0f);
    fW_Edge = 0;
    _height = CGScale(36.0f) ;
    _toal = -1;
    m_Data = [NSMutableDictionary dictionary];
    current_Button = nil;
    _selectedIndex = -1;
}

/**
 * @brief 设置背影图片
 * @param img 设置的图片
 */
- (void) setBackgroundImage:(UIImage *) img
{
    if (ivBKG && img)
    {
        ivBKG = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:ivBKG];
    }
    [self sendSubviewToBack:ivBKG];
    ivBKG.image = img;
}

- (void) setContentColor:(UIColor *) m_Color state:(MLSelectState) state
{
    NSMutableArray *tmp = [NSMutableArray array];
    for (int i = 0; i < _toal; i++) [tmp addObject:m_Color];
    
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:tmp forKey:kMLSelectStateNormal_Content_Color];
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:tmp forKey:kMLSelectStateSelected_Content_Color];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:tmp forKey:kMLSelectStateHighlighted_Content_Color];
            break;
            
        default:
            break;
    }

}

/**
 * @brief 设置控件的标体颜色 (所有)
 * @param m_Color 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitleColor:(UIColor *) m_Color state:(MLSelectState) state
{
    NSMutableArray *tmp = [NSMutableArray array];
    for (int i = 0; i < _toal; i++) [tmp addObject:m_Color];
    
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:tmp forKey:kMLSelectStateNormal_Color];
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:tmp forKey:kMLSelectStateSelected_Color];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:tmp forKey:kMLSelectStateHighlighted_Color];
            break;
            
        default:
            break;
    }
}


/**
 * @brief 设置控件的标题
 * @param m_Title 他是一个标题数组 如果没有设置 toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitles:(NSArray *) m_Title state:(MLSelectState) state
{
    [m_Data removeAllObjects];
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:m_Title forKey:kMLSelectStateNormal];
            if (_toal == -1) _toal = m_Title.count;
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:m_Title forKey:kMLSelectStateSelected];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:m_Title forKey:kMLSelectStateHighlighted];
            break;
            
        default:
            break;
    }
}


/**
 * @brief 设置控件的标题颜色
 * @param m_Color 他是颜色数组 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitleColors:(NSArray *) m_Color state:(MLSelectState) state
{
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:m_Color forKey:kMLSelectStateNormal_Color];
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:m_Color forKey:kMLSelectStateSelected_Color];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:m_Color forKey:kMLSelectStateHighlighted_Color];
            break;
            
        default:
            break;
    }
}


/**
 * @brief 设置控件的标题和标题颜色
 * @param m_Title 他是一个标题数组  toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param m_Color 他是颜色数组 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitles:(NSArray *) m_Title color:(NSArray *) m_Color state:(MLSelectState) state
{
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:m_Title forKey:kMLSelectStateNormal];
            [m_Data setObject:m_Color forKey:kMLSelectStateNormal_Color];
            if (_toal == -1) _toal = m_Title.count;
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:m_Title forKey:kMLSelectStateSelected];
            [m_Data setObject:m_Color forKey:kMLSelectStateSelected_Color];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:m_Title forKey:kMLSelectStateHighlighted];
            [m_Data setObject:m_Color forKey:kMLSelectStateHighlighted_Color];
            break;
            
        default:
            break;
    }
}


/**
 * @brief 设置控件的背景
 * @param m_Title 他是一个标题数组 如果没有设置 toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param state 他是一个状态标记显示的类型
 */
- (void) setBackgroundImages:(NSArray *) m_Title state:(MLSelectState) state
{
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:m_Title forKey:kMLSelectStateNormal_Img];
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:m_Title forKey:kMLSelectStateSelected_Img];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:m_Title forKey:kMLSelectStateHighlighted_Img];
            break;
            
        default:
            break;
    }
}

/**
 * @brief 设置控件的背景 (所有)
 * @param m_Title 如果没有设置 toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param state 他是一个状态标记显示的类型
 */
- (void) setBackgroundImage:(UIImage *) m_Title state:(MLSelectState) state
{
    if (!m_Title) return;
    
    NSMutableArray *tmp = [NSMutableArray array];
    for (int i = 0; i < _toal; i++) [tmp addObject:m_Title];
    
    switch (state) {
        case MLSelectStateNormal:
            [m_Data setObject:tmp forKey:kMLSelectStateNormal_Img];
            break;
            
        case MLSelectStateSelected:
            [m_Data setObject:tmp forKey:kMLSelectStateSelected_Img];
            break;
            
        case MLSelectStateHighlighted:
            [m_Data setObject:tmp forKey:kMLSelectStateHighlighted_Img];
            break;
            
        default:
            break;
    }
}


- (NSAttributedString *) contentWithTitle:(NSString *) title color:(UIColor *) color{
    NSString *strTmp = [NSString stringWithFormat:@"%@", title];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:strTmp];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = CGScale(19.0f);
    style.maximumLineHeight = style.minimumLineHeight;
    style.alignment = NSTextAlignmentCenter;
    [str addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, str.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfBaseSize:15.0f] range:NSMakeRange(0, 2)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfBaseSize:13.0f] range:NSMakeRange(2, str.length - 2)];
    
    [str addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, str.length)];
    return str;
}
 

/**
 * @brief showLoadView  显示按扭界面
 * @return  返回load是否成功
 */
- (BOOL) showLoadView
{
    if (_toal <= 0) return NO;
    
    // 清楚以前的
    [vButton.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    // load button
    float but_width = (VIEWW(self) - _edge_width * 2)/_toal;

    NSArray *m_title_Normal = [m_Data objectForKey:kMLSelectStateNormal];
    NSArray *m_title_Selected = [m_Data objectForKey:kMLSelectStateSelected];
    NSArray *m_title_Highlighted = [m_Data objectForKey:kMLSelectStateHighlighted];

    NSArray *m_img_Normal = [m_Data objectForKey:kMLSelectStateNormal_Img];
    NSArray *m_img_Selected = [m_Data objectForKey:kMLSelectStateSelected_Img];
    NSArray *m_img_Highlighted = [m_Data objectForKey:kMLSelectStateHighlighted_Img];
    
    NSArray *m_color_Normal = [m_Data objectForKey:kMLSelectStateNormal_Color];
    NSArray *m_color_Selected = [m_Data objectForKey:kMLSelectStateSelected_Color];
    NSArray *m_color_Highlighted = [m_Data objectForKey:kMLSelectStateHighlighted_Color];
    
    NSArray *m_content_color_Normal = [m_Data objectForKey:kMLSelectStateNormal_Content_Color];
//    NSArray *m_content_color_Selected = [m_Data objectForKey:kMLSelectStateSelected_Content_Color];
//    NSArray *m_content_color_Highlighted = [m_Data objectForKey:kMLSelectStateHighlighted_Content_Color];
    
    // add
    for (int i = 0; i < _toal; i++) {
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        but.frame = CGRectMake(_edge_width + (but_width * i), CGScale(1.0f), but_width, _height - CGScale(1.0f));
        if (_titleFont) but.titleLabel.font= _titleFont;
        else but.titleLabel.font=[UIFont systemFontOfBaseSize:14.0f];
        but.titleLabel.numberOfLines = 0;
        but.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        // title
        if (_isAttributedTitle) {
            if (m_title_Normal && m_title_Normal.count >= _toal) {
                [but setAttributedTitle:[self contentWithTitle:[m_title_Normal objectAtIndex:i] color:__RGB(0x000000)] forState:UIControlStateNormal];
                [but setAttributedTitle:[self contentWithTitle:[m_title_Normal objectAtIndex:i] color:__RGB(0xff662a)] forState:UIControlStateSelected];
                [but setAttributedTitle:[self contentWithTitle:[m_title_Normal objectAtIndex:i] color:__RGB(0xff662a)] forState:UIControlStateHighlighted];
            }
            
            if (m_title_Selected && m_title_Selected.count >= _toal)
                [but setAttributedTitle:[self contentWithTitle:[m_title_Normal objectAtIndex:i] color:__RGB(0xff662a)] forState:UIControlStateSelected];
            
            if (m_title_Highlighted)
                [but setAttributedTitle:[self contentWithTitle:[m_title_Normal objectAtIndex:i] color:__RGB(0xff662a)] forState:UIControlStateHighlighted];

        } else {
            if (m_title_Normal && m_title_Normal.count >= _toal) {
                [but setTitle:[m_title_Normal objectAtIndex:i] forState:UIControlStateNormal];
                [but setTitle:[m_title_Normal objectAtIndex:i] forState:UIControlStateSelected];
                [but setTitle:[m_title_Normal objectAtIndex:i] forState:UIControlStateHighlighted];
            }
            
            if (m_title_Selected && m_title_Selected.count >= _toal)
                [but setTitle:[m_title_Selected objectAtIndex:i] forState:UIControlStateSelected];
            
            if (m_title_Highlighted)
                [but setTitle:[m_title_Highlighted objectAtIndex:i] forState:UIControlStateHighlighted];
        }
        // color
        if (m_color_Normal && m_color_Normal.count >= _toal)
            [but setTitleColor:[m_color_Normal objectAtIndex:i] forState:UIControlStateNormal];
        
        if (m_color_Selected && m_color_Selected.count >= _toal)
            [but setTitleColor:[m_color_Selected objectAtIndex:i] forState:UIControlStateSelected];
        
        if (m_color_Highlighted && m_color_Highlighted.count >= _toal)
            [but setTitleColor:[m_color_Highlighted objectAtIndex:i] forState:UIControlStateHighlighted];
        
        // backgroud image
        if (m_img_Normal && m_img_Normal.count >= _toal)
            [but setBackgroundImage:[m_img_Normal objectAtIndex:i] forState:UIControlStateNormal];

        if (m_img_Selected && m_img_Selected.count >= _toal)
            [but setBackgroundImage:[m_img_Selected objectAtIndex:i] forState:UIControlStateSelected];
        
        if (m_img_Highlighted && m_img_Highlighted.count >= _toal)
            [but setBackgroundImage:[m_img_Highlighted objectAtIndex:i] forState:UIControlStateHighlighted];
        
        
        // content background
        if (m_content_color_Normal && m_content_color_Normal.count >= _toal)
            [but setBackgroundColor:[m_content_color_Normal objectAtIndex:i]];        
        
        
        
        
//        but.backgroundColor=[UIColor redColor];
        
        but.tag = (Offset_TAG + i);
        [but addTarget:self action:@selector(didTitleButtonPrass:) forControlEvents:UIControlEventTouchUpInside];
        [but addTarget:self action:@selector(didTitleButtonDown:) forControlEvents:UIControlEventTouchDown];
        [but addTarget:self action:@selector(didTitleButtonUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
        [but addTarget:self action:@selector(didTitleButtonCancel:) forControlEvents:UIControlEventTouchCancel];
        [but addTarget:self action:@selector(didTitleButtonAllTouch:) forControlEvents:UIControlEventAllTouchEvents];
        [vButton addSubview:but];
    }
    
    [self setSelectedIndex:_selectedIndex];

    return YES;
}

- (void) didTitleButtonAllTouch:(UIButton *) button {
    if (button.selected) button.highlighted = NO;
}

- (void) didTitleButtonCancel:(UIButton *) button
{
    for (UIButton *but in vButton.subviews) but.userInteractionEnabled = YES;// [but setEnabled:YES];
    if (current_Button) current_Button.selected = YES;
}

- (void) didTitleButtonDown:(UIButton *) button
{
    //重复点击
    for (UIButton *but in vButton.subviews) {
        if (![but isEqual:button]) but.userInteractionEnabled = NO; // [but setEnabled:NO];
    }
}
- (void) didTitleButtonUpOutside:(UIButton *) button
{
    for (UIButton *but in vButton.subviews) but.userInteractionEnabled = YES;// [but setEnabled:YES];
    if (current_Button) current_Button.selected = YES;
}

- (void) didTitleButtonPrass:(UIButton *) button
{
    //重复点击
    for (UIButton *but in vButton.subviews) but.userInteractionEnabled = YES;// [but setEnabled:YES];
    
    
//    NSArray *m_content_color_Normal = [m_Data objectForKey:kMLSelectStateNormal_Content_Color];
//    NSArray *m_content_color_Selected = [m_Data objectForKey:kMLSelectStateSelected_Content_Color];
//    current_Button.backgroundColor = (m_content_color_Normal.count > 0 ? [m_content_color_Normal firstObject] : [UIColor clearColor]);
//    button.backgroundColor = (m_content_color_Selected.count > 0 ? [m_content_color_Selected firstObject] : [UIColor clearColor]);
    
    if (current_Button)  current_Button.selected = NO;
    current_Button = button;
    current_Button.selected = YES;
    
    self.selectedIndex = button.tag - Offset_TAG;
    
    if ([_delegate respondsToSelector:@selector(MLNavViewBar:didSelectIndex:)])
        [_delegate MLNavViewBar:self didSelectIndex:button.tag - Offset_TAG];
    
    if (_didSelectAtIndex) _didSelectAtIndex((int)(button.tag - Offset_TAG));
}

/**
 * @brief 设置选择na个
 * @param index 设置选择（从0 开始 小于 toal）
 */
- (void) setSelectedIndex:(NSInteger) index
{
    _selectedIndex = index;
    if (_selectedIndex == -1) _selectedIndex = 0;

    if (_selectedIndex >= 0 && _selectedIndex < vButton.subviews.count) {
        if (current_Button)  current_Button.selected = NO;
        
        UIButton *but = (UIButton *)[vButton viewWithTag:(_selectedIndex + Offset_TAG)];
        current_Button = but;
        current_Button.selected = YES;
    }
}

- (void) setIndex:(int) index {
    if (_selectedIndex >= 0 && _selectedIndex < vButton.subviews.count) {
        UIButton *but = (UIButton *)[vButton viewWithTag:(index + Offset_TAG)];
        if (but) [self didTitleButtonPrass:but];
    }
}

@end
