//
//  MLPageView.m
//

#import "MLPageView.h"
#import "MLPageControl.h"

@interface MLPageView () {
    MLPageControl *_pageControl;
    UILabel *_labTitle;
}

@end

@implementation MLPageView

@synthesize dataSource;
@synthesize delegate;
@synthesize m_Items;
@synthesize style;
@synthesize currentView;

- (instancetype) init {
    if (self = [super init]) {
        isAutoPage = YES;
        style = MLPageViewStyleDefault;
        pageStatus = MLPageViewStatusNone;
        
        [self buildView];
    }
    return self;
}

#pragma mark - setIsTranslucent
- (void) setIsTranslucent:(BOOL)isTranslucent {
    _isTranslucent = isTranslucent;
    _scrollView.clipsToBounds = isTranslucent;
}

#pragma mark - view
- (void) buildView
{
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.clipsToBounds = YES;
    _scrollView.delegate = self;
    [self addSubview:_scrollView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionClickGesture:)];
    [self addGestureRecognizer:singleTap];
    
    [self loadDefualtView];
}


- (void) loadDefualtView
{
    if (!currentView) {
        preView = [[UIView alloc] init];
        currentView = [[UIView alloc] init];
        nextView = [[UIView alloc] init];
        
        preView.backgroundColor = currentView.backgroundColor = nextView.backgroundColor = [UIColor clearColor];
        
        [_scrollView addSubview:preView];
        [_scrollView addSubview:nextView];
        [_scrollView addSubview:currentView];
    }
}

#pragma mark - reloadData
- (void) reloadData
{
    if (!dataSource) return;
    
    _totalPages = (int)[dataSource numberOfItemsInPageView:self];
    m_Items = [NSMutableDictionary dictionaryWithCapacity:_totalPages];
    
    
    isBounds = NO;
    if ([dataSource respondsToSelector:@selector(pageViewIsBoundsInTarget:)])
        isBounds = [dataSource pageViewIsBoundsInTarget:self];
    
    
    if (_totalPages < 1) return;
    else if (_totalPages == 1) _scrollView.scrollEnabled = NO;
    else _scrollView.scrollEnabled = YES;
    
    _curPage = 0;
    
    [self loadStyle];
    [self loadDefualtView];
    
    if (isBounds) {
        [self setChildFrameWithSuperBounds:self.bounds];
        [_scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
    }
    else{
        
        [self setChildFrameWithSuperBounds:self.bounds];
        [_scrollView setContentOffset:CGPointMake(VIEWW(_scrollView), 0.0f) animated:NO];
        
        [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:preView];
        [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:nextView];
        [self loadBufferItemViewAtIndex:_curPage bufView:currentView];
    }
}

- (UIView *) loadBufferItemViewAtIndex:(NSInteger) index bufView:(UIView *) bufView
{
    UIView *view = nil;
    if ([dataSource respondsToSelector:@selector(pageView:viewForItemAtIndex:resetView:)])
        view = [dataSource pageView:self viewForItemAtIndex:index resetView:[self itemViewAtIndex:index]];
    
    NSAssert(view != nil, @"不可以设置这个为nil");
    
    if (isBounds) {
        [bufView removeFromSuperview];
        [_scrollView addSubview:view];
    }
    else  {
        for (UIView *v in bufView.subviews) [v removeFromSuperview];
//        [bufView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [bufView addSubview:view];
    }
    [self setShowView:view forIndex:index];
    
    return view;
}

#pragma mark - set layout view
- (void) loadStyle
{
    if ([dataSource respondsToSelector:@selector(pageViewTypeInTarget:)])
        style = [dataSource pageViewTypeInTarget:self];
    
    if (style == MLPageViewStyleNormal) {

    }
    else if (style == MLPageViewStylePage)
    {
        if (!_pageControl) {
            _pageControl = [[MLPageControl alloc] init];
            _pageControl.wPoint = 5.0f;
            _pageControl.pageIndicatorTintColor = __RGB(0x92cbe5);
            _pageControl.currentPageIndicatorTintColor = __RGB(0x2698cb);
            [self addSubview:_pageControl];
        }
        _pageControl.numberOfPages = _totalPages;
        _pageControl.currentPage = 0;
    }
    else if (style == MLPageViewStyleBottom) {
        if (!_pageControl) {
            _pageControl = [[MLPageControl alloc] init];
            _pageControl.wPoint = 5.0f;
            _pageControl.pageIndicatorTintColor = __RGB(0x92cbe5);
            _pageControl.currentPageIndicatorTintColor = __RGB(0x2698cb);
            [self addSubview:_pageControl];
        }
        _pageControl.numberOfPages = _totalPages;
        _pageControl.currentPage = 0;

        _ivBKG = [[UIImageView alloc] init];
        _ivBKG.backgroundColor = __RGB(0x166783);
        _ivBKG.layer.borderColor = __RGB(0xaac8d3).CGColor;
        _ivBKG.layer.borderWidth = 0.5;
        _ivBKG.layer.masksToBounds = YES;
        [self addSubview:_ivBKG];
        
        _ivSelectBKG = [[UIImageView alloc] init];
        _ivSelectBKG.backgroundColor = __RGB(0x4fd1ff);
        [_ivBKG addSubview:_ivSelectBKG];
    }
    else if (style == MLPageViewStyleBottomCenter) {
        if (!_pageControl) {
            _pageControl = [[MLPageControl alloc] init];
            _pageControl.wPoint = 5.0f;
            _pageControl.pageIndicatorTintColor = __RGB(0x92cbe5);
            _pageControl.currentPageIndicatorTintColor = __RGB(0x2698cb);
            [self addSubview:_pageControl];
        }
        _pageControl.numberOfPages = _totalPages;
        _pageControl.currentPage = 0;
    }
    else if (style == MLPageViewStylePageTitle) {
        if (!_labTitle) {
            _labTitle = [[UILabel alloc] init];
            _labTitle.textColor = [UIColor whiteColor];
            _labTitle.font = [UIFont systemFontOfSize:12.0f];
            _labTitle.textAlignment = NSTextAlignmentRight;
            _labTitle.shadowColor = __RGBA(0x000000, 0.8);
            _labTitle.shadowOffset = CGSizeMake(1.0f, 1.0f);
            
            [self addSubview:_labTitle];
        }
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d张", _totalPages]];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:NSMakeRange(0, str.length - 1)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(str.length - 1, 1)];
        _labTitle.attributedText = str;
    }
   
    
    _curPage = 0;
}


#pragma mark - frame
- (void) layoutSubviews {
    [super layoutSubviews];
    [self reloadData];
}

- (void) setChildFrameWithSuperBounds:(CGRect) rect {
    
    if (style == MLPageViewStyleNormal) {
        if (_ivBKG) {[_ivBKG removeFromSuperview]; _ivBKG = nil;}
        if (_ivSelectBKG) {[_ivSelectBKG removeFromSuperview]; _ivSelectBKG = nil;}
        if (_pageControl) {[_pageControl removeFromSuperview]; _pageControl = nil;}
        if (_labTitle) {[_labTitle removeFromSuperview]; _labTitle = nil;}
        
        _scrollView.frame = self.bounds;
    }
    else if (style == MLPageViewStylePage) {
        if (_ivBKG) {[_ivBKG removeFromSuperview]; _ivBKG = nil;}
        if (_ivSelectBKG) {[_ivSelectBKG removeFromSuperview]; _ivSelectBKG = nil;}
        if (_labTitle) {[_labTitle removeFromSuperview]; _labTitle = nil;}
        
        _scrollView.frame = self.bounds;
        if (CGRectIsEmpty(_pageFrame)) _pageControl.frame = CGRectMake(13.0f, rect.size.height - 23.0f, 36.0f, 12.0f);
        else _pageControl.frame = _pageFrame;
    }
    else if (style == MLPageViewStyleBottom) {
        if (_labTitle) {[_labTitle removeFromSuperview]; _labTitle = nil;}
        
        CGFloat width = rect.size.width/_totalPages;
        _ivBKG.frame = CGRectMake(-0.5f, rect.size.height - 5.5f, rect.size.width + 1.0f, 5.5f);
        _ivSelectBKG.frame = CGRectMake(0.0f, 0.5f, width, 4.5f);
        _scrollView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height - 5.5f);
        if (CGRectIsEmpty(_pageFrame)) _pageControl.frame = CGRectMake(13.0f, rect.size.height - 23.0f, 36.0f, 12.0f);
        else _pageControl.frame = _pageFrame;
    }
    else if (style == MLPageViewStyleBottomCenter) {
        if (_ivBKG) {[_ivBKG removeFromSuperview]; _ivBKG = nil;}
        if (_ivSelectBKG) {[_ivSelectBKG removeFromSuperview]; _ivSelectBKG = nil;}
        if (_labTitle) {[_labTitle removeFromSuperview]; _labTitle = nil;}
        
        _scrollView.frame = self.bounds;
        if (CGRectIsEmpty(_pageFrame)) _pageControl.frame = CGRectMake((VIEWW(self) - 36.0f)/2.0f, rect.size.height - 13.0f, 36.0f, 12.0f);
        else _pageControl.frame = _pageFrame;
    }
    else if (style == MLPageViewStylePageTitle) {
        if (_ivBKG) {[_ivBKG removeFromSuperview]; _ivBKG = nil;}
        if (_ivSelectBKG) {[_ivSelectBKG removeFromSuperview]; _ivSelectBKG = nil;}
        if (_pageControl) {[_pageControl removeFromSuperview]; _pageControl = nil;}
        
        _scrollView.frame = self.bounds;
        if (CGRectIsEmpty(_pageFrame)) _labTitle.frame = CGRectMake(13.0f, rect.size.height - 23.0f, 36.0f, 22.0f);
        else _labTitle.frame = _pageFrame;
    }
    
    
    if (isBounds) {
        _scrollView.contentSize = CGSizeMake(_scrollView.bounds.size.width * _totalPages, 0.0f);
        
        UIView *vPre = [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:[self itemViewAtIndex:[self preAtIndex]]];
        UIView *vCur = [self loadBufferItemViewAtIndex:_curPage bufView:[self itemViewAtIndex:_curPage]];
        UIView *vNext = [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:[self itemViewAtIndex:[self nextAtIndex]]];
        
        vPre.frame = CGRectMake(VIEWW(_scrollView) * [self preAtIndex], 0.0, VIEWH(_scrollView), VIEWH(_scrollView));
        vCur.frame = CGRectMake(VIEWW(_scrollView)*_curPage, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        vNext.frame = CGRectMake(VIEWW(_scrollView)*[self nextAtIndex], 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        
    } else {
        _scrollView.contentSize = CGSizeMake(_scrollView.bounds.size.width * 3, 0.0f);
        
        preView.frame = CGRectMake(0.0, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        currentView.frame = CGRectMake(VIEWW(_scrollView), 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        nextView.frame = CGRectMake(VIEWW(_scrollView)*2, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
    }
}


#pragma mark - UIScrollViewDelegate
- (void) scrollViewDidScroll:(UIScrollView *) scrollView {
    if (isAutoPage) return;
    
    CGFloat page = scrollView.contentOffset.x/scrollView.bounds.size.width;
    pageStatus = floor(page);
    if (isBounds) {
        if (page > 0 && page < 1)  pageStatus = MLPageViewStatusNone;
        if (pageStatus == MLPageViewStatusNext) {
            if ([delegate respondsToSelector:@selector(pageView:allEventsAtIndex:)])
                [delegate pageView:self allEventsAtIndex:_curPage];
            
            if ([delegate respondsToSelector:@selector(pageView:didScrollViewDidEndAtIndex:)])
                [delegate pageView:self didScrollViewDidEndAtIndex:_curPage];
        }
    }else {
        if (page > 0 && page < 1)  pageStatus = MLPageViewStatusNone;
        if (pageStatus == MLPageViewStatusPre) [self setShowViewAtPage:[self preAtIndex] isAuto:NO];
        else if (pageStatus == MLPageViewStatusNext) [self setShowViewAtPage:[self nextAtIndex] isAuto:NO];
    }
}

// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (isPlayPage) {
        if ([timer isValid]) {
            [timer invalidate];
            [timer fire];
            timer = nil;
        }
    }
    isAutoPage = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (isPlayPage) [self startAnimations];
    isAutoPage = YES;
    
    if (isBounds) {
        CGFloat page = scrollView.contentOffset.x/scrollView.bounds.size.width;
        _curPage = floor(page);
        
        UIView *vPre = [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:[self itemViewAtIndex:[self preAtIndex]]];
        UIView *vCur = [self loadBufferItemViewAtIndex:_curPage bufView:[self itemViewAtIndex:_curPage]];
        UIView *vNext = [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:[self itemViewAtIndex:[self nextAtIndex]]];
        
        vPre.frame = CGRectMake(VIEWW(_scrollView) * [self preAtIndex], 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        vCur.frame = CGRectMake(VIEWW(_scrollView)*_curPage, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
        vNext.frame = CGRectMake(VIEWW(_scrollView)*[self nextAtIndex], 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
    
        
        // ================================================================================= 9x981n
        if (style == MLPageViewStyleNormal) {
            
        }
        else if (style == MLPageViewStylePage) {
            _pageControl.currentPage = _curPage;
            
        }
        else if (style == MLPageViewStyleBottom) {
            _pageControl.currentPage = _curPage;
            if (_ivSelectBKG) {
                CGRect rect = _ivSelectBKG.frame;
                rect.origin.x =  (VIEWW(_scrollView) * ((CGFloat)_curPage/(CGFloat)_totalPages)) + 0.5;
                _ivSelectBKG.frame = rect;
            }
        }
        else if (style == MLPageViewStyleBottomCenter) {
            _pageControl.currentPage = _curPage;
        }
        else if (style == MLPageViewStylePageTitle) {
            
        }
    }
}


#pragma mark - methon
- (int) preAtIndex
{
    return (int)( (_curPage <= 0) ? (_totalPages - 1) : (_curPage - 1));
}

- (int) nextAtIndex
{
    return (int)((_curPage >= (_totalPages - 1)) ? 0 : (_curPage + 1));
}

- (void) setShowViewAtPage:(int) page isAuto:(BOOL) isAuto
{
    if (page == _curPage) {
        pageStatus = MLPageViewStatusNone;
    }
    else if (page > _curPage) {
        pageStatus = MLPageViewStatusNext;
        UIView *vTmp = preView;
        preView = currentView;
        currentView = nextView;
        nextView = vTmp;
        
    } else if (page < _curPage) {
        pageStatus = MLPageViewStatusPre;
        UIView *vTmp = nextView;
        nextView = currentView;
        currentView = preView;
        preView = vTmp;
    }

    // ================================================================================= 9x981n
    preView.frame = CGRectMake(0.0, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
    currentView.frame = CGRectMake(VIEWW(_scrollView), 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
    nextView.frame = CGRectMake(VIEWW(_scrollView)*2, 0.0, VIEWW(_scrollView), VIEWH(_scrollView));
    
    // ================================================================================= 9x981n
    _curPage = page;
    
    [self loadBufferItemViewAtIndex:_curPage bufView:currentView];
    [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:preView];
    [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:nextView];
    
    if (isAuto) {
        [_scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
        [_scrollView setContentOffset:CGPointMake(VIEWW(_scrollView), 0.0f) animated:YES];
    }
    else
    {
        [_scrollView setContentOffset:CGPointMake(VIEWW(_scrollView), 0.0f) animated:NO];
    }

    
    // ================================================================================= 9x981n
    if (style == MLPageViewStyleNormal) {
        
    }
    else if (style == MLPageViewStylePage) {
        _pageControl.currentPage = _curPage;
    
    }
    else if (style == MLPageViewStyleBottom) {
        _pageControl.currentPage = _curPage;
        if (_ivSelectBKG) {
            CGRect rect = _ivSelectBKG.frame;
            rect.origin.x =  (VIEWW(_scrollView) * ((CGFloat)_curPage/(CGFloat)_totalPages)) + 0.5;
            _ivSelectBKG.frame = rect;
        }
    }
    else if (style == MLPageViewStyleBottomCenter) {
        _pageControl.currentPage = _curPage;
    }
    else if (style == MLPageViewStylePageTitle) {

    }
}

#pragma mark - cycle animation
- (void) cycleAnimation
{
    if (!isAutoPage) return;
    [self setShowViewAtPage:[self nextAtIndex] isAuto:YES];
}

- (void) startAnimations
{
    [self stopAnimations];
    
    if (!timer) {
        timer = [NSTimer timerWithTimeInterval:4.5 target:self selector:@selector(cycleAnimation) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
    isPlayPage = YES;
}

- (void) stopAnimations {
    if ([timer isValid]) {
        [timer invalidate];
        [timer fire];
        timer = nil;
    }
    isPlayPage = NO;
}

#pragma mark - tap event
- (void) actionClickGesture:(UIGestureRecognizer *) gesture {
    if ([delegate respondsToSelector:@selector(pageView:didSelectPageAtIndex:)]) {
        [delegate pageView:self didSelectPageAtIndex:_curPage];
    }
    
    if ([delegate respondsToSelector:@selector(pageView:allEventsAtIndex:)]) {
        [delegate pageView:self allEventsAtIndex:_curPage];
    }
}

#pragma mark - 保存的界面
- (UIView *) itemViewAtIndex:(NSInteger) index
{
    return [m_Items objectForKey:[NSNumber numberWithInteger:index]];
}

- (void) setShowView:(UIView *)view forIndex:(NSInteger)index
{
    [m_Items setObject:view forKey:[NSNumber numberWithInteger:index]];
}

@end
