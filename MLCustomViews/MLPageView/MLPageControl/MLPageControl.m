//
//  MLPageControl.m
//

#import "MLPageControl.h"

@implementation MLPageControl

@synthesize wPoint;

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
//        self.pageIndicatorTintColor = [UIColor colorWithRed:149/255.0f green:149/255.0f blue:149/125.0f alpha:1];
//        self.currentPageIndicatorTintColor =  [UIColor colorWithRed:22/255.0f green:99/255.0f blue:126/125.0f alpha:1];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (wPoint == 0) return;
    
    CGFloat x = (self.bounds.size.width - (self.numberOfPages * wPoint*2))/2.0f;
    int i = 0;
    for (UIView *v in self.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            v.frame = CGRectMake(x + (wPoint*2 * i), (self.bounds.size.height - wPoint)/2.0f, wPoint, wPoint);
            v.layer.cornerRadius = wPoint/2.0f;
            i++;
        }
    }
}

@end
