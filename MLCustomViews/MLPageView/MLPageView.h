//
//  MLPageView.h
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MLPageViewStyle) {
    MLPageViewStyleNormal = 0, // 空的什么都没有
    MLPageViewStylePage,        // 显示.的进度
    MLPageViewStyleBottom,      // 显示底部由分页进度
    MLPageViewStyleBottomCenter,
    MLPageViewStylePageTitle,   // 显示文字的进度
    MLPageViewStyleDefault = MLPageViewStyleNormal,
};

typedef NS_ENUM(NSInteger, MLPageViewStatus) {
    MLPageViewStatusNone = 1,
    MLPageViewStatusPre = 0,
    MLPageViewStatusNext = 2,
};


@class MLPageView, MLPageControl;
@protocol MLPageViewDataSource <NSObject>

- (NSUInteger) numberOfItemsInPageView:(MLPageView *) pageView;
- (UIView *) pageView:(MLPageView *) pageView viewForItemAtIndex:(NSUInteger) index resetView:(UIView *) view;

@optional

// LXTableViewCellTyleDefault
- (MLPageViewStyle) pageViewTypeInTarget:(MLPageView *) pageView;

//shi fu hua dong dao bian
- (BOOL) pageViewIsBoundsInTarget:(MLPageView *) pageView;

@end

@protocol MLPageViewDelegate <NSObject>
@optional

- (void) pageView:(MLPageView *) pageview didSelectPageAtIndex:(NSInteger) index;

- (void) pageView:(MLPageView *) pageview didScrollViewDidEndAtIndex:(NSInteger) index;

- (void) pageView:(MLPageView *) pageview allEventsAtIndex:(NSInteger) index;

@end


@interface MLPageView : UIView <UIScrollViewDelegate>
{
    @package
    UIScrollView *_scrollView;
    
    UIView *preView;
    UIView *currentView;
    UIView *nextView;
    
    //
    int _totalPages;
    int _curPage;
    
    NSTimer *timer;
    BOOL isAutoPage;
    BOOL isPlayPage;
    MLPageViewStatus pageStatus;
    
    BOOL isBounds;
}

@property (nonatomic, assign) id <MLPageViewDataSource> dataSource;

@property (nonatomic, assign) id <MLPageViewDelegate> delegate;

@property (nonatomic, strong, readonly) NSMutableDictionary *m_Items;

@property (nonatomic, assign, readonly) MLPageViewStyle style;

@property (nonatomic, strong, readonly) UIView *currentView;

@property (nonatomic, assign) BOOL isTranslucent;

// 设置样式的一种
@property (nonatomic, strong) UIImageView *ivBKG;
@property (nonatomic, strong) UIImageView *ivSelectBKG;

// 设置样式显示的位置
@property (nonatomic, assign) CGRect pageFrame;

- (void) startAnimations;
- (void) stopAnimations;

// 刷新数据
- (void) reloadData;

@end
