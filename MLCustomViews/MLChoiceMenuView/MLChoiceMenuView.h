//
//  MLChoiceMenuView.h
//

#import <UIKit/UIKit.h>


@interface MLChoiceModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int index;
@property (nonatomic, strong) id objc;

@property (nonatomic, strong) NSArray *next;

@end


typedef void(^PickerViewBlock)(int first, int second, int third, id objc);
typedef void(^ChoiceBlock)(id objc);

@interface MLChoiceMenuView : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
{
    @package
    UIView *_backgroundView;
    UIView *_mainView;
    
    UIPickerView *_pickerView;
    UIButton *_butCancel;
    
    PickerViewBlock complete;
    ChoiceBlock choice;
    NSArray *m_AData;
    
    int comp;
}

+ (instancetype) sharedInstance;
+ (void) showPickerData:(NSArray *) m_AData block:(PickerViewBlock) complete;
+ (void) showOnePickerData:(NSArray *) m_AData block:(ChoiceBlock) complete;


@end
